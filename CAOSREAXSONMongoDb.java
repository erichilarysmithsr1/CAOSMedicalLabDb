package .;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

public class Types {
  public static class FoodAccessHouseholdInsertInput {
    private FoodAccessHouseholdCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public FoodAccessHouseholdInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = new FoodAccessHouseholdCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public FoodAccessHouseholdCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class MentalHealthFaqInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public MentalHealthFaqInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class GupsyHospitalUpdateInput {
    private Boolean _CAOS2_partitionKey_unset;
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private GupsyHospitalCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
  
    public GupsyHospitalUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = new GupsyHospitalCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
      }
    }
  
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public GupsyHospitalCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class CholesterolUpdateInput {
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
  
    public CholesterolUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
  }
  
  
  
  public static class YaleFoodAddictionScaleQueryInput {
    private Object _CAOS2_partitionKey_gte;
    private String _CAOS1_partitionKey;
    private String _CAOS1_partitionKey_gte;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_lt;
    private String _CAOS1_partitionKey_lt;
    private Object _CAOS2_partitionKey_ne;
    private String _CAOS1_partitionKey_ne;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_gt;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Boolean _CAOS1_partitionKey_exists;
    private Iterable<YaleFoodAddictionScaleQueryInput> _OR;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private String _CAOS1_partitionKey_gt;
    private String _CAOS1_partitionKey_lte;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<YaleFoodAddictionScaleQueryInput> _AND;
  
    public YaleFoodAddictionScaleQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(YaleFoodAddictionScaleQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(YaleFoodAddictionScaleQueryInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Iterable<YaleFoodAddictionScaleQueryInput> getOr() { return this._OR; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<YaleFoodAddictionScaleQueryInput> getAnd() { return this._AND; }
  }
  
  public static class McDatumQueryInput {
    private String _CAOS1_partitionKey_gte;
    private String _CAOS1_partitionKey_ne;
    private String _CAOS1_partitionKey_gt;
    private Boolean _CAOS2_partitionKey_exists;
    private Iterable<McDatumQueryInput> _AND;
    private FriedFoodsMortalityQueryInput _CAOS2_partitionKey;
    private Iterable<McDatumQueryInput> _OR;
    private String _CAOS1_partitionKey;
    private String _CAOS1_partitionKey_lte;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey_lt;
    private Iterable<String> _CAOS1_partitionKey_in;
  
    public McDatumQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(McDatumQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(McDatumQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
      }
    }
  
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Iterable<McDatumQueryInput> getAnd() { return this._AND; }
    public FriedFoodsMortalityQueryInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Iterable<McDatumQueryInput> getOr() { return this._OR; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
  }
  
  
  public enum CholineSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    CholineSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, CholineSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (CholineSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static CholineSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class McDatumCaos2_PartitionKeyRelationInput {
    private Object _link;
    private FriedFoodsMortalityInsertInput _create;
  
    public McDatumCaos2_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._link = (Object) args.get("link");
        this._create = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("create"));
      }
    }
  
    public Object getLink() { return this._link; }
    public FriedFoodsMortalityInsertInput getCreate() { return this._create; }
  }
  public static class YaleFoodAddictionScaleInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public YaleFoodAddictionScaleInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class BetaCaroteneQueryInput {
    private Object _CAOS2_partitionKey_ne;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private Object _CAOS2_partitionKey_gte;
    private String _CAOS1_partitionKey;
    private Iterable<BetaCaroteneQueryInput> _AND;
    private String _CAOS1_partitionKey_gt;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_gt;
    private Iterable<BetaCaroteneQueryInput> _OR;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey_ne;
    private Object _CAOS2_partitionKey_lte;
    private String _CAOS1_partitionKey_lt;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private String _CAOS1_partitionKey_lte;
    private Boolean _CAOS2_partitionKey_exists;
    private String _CAOS1_partitionKey_gte;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Object _CAOS2_partitionKey_lt;
  
    public BetaCaroteneQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(BetaCaroteneQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(BetaCaroteneQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
      }
    }
  
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Iterable<BetaCaroteneQueryInput> getAnd() { return this._AND; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Iterable<BetaCaroteneQueryInput> getOr() { return this._OR; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
  }
  public static class BetaCaroteneInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public BetaCaroteneInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class NutritionixGroceryListCaos1_PartitionKeyRelationInput {
    private FoodAccessHouseholdInsertInput _create;
    private String _link;
  
    public NutritionixGroceryListCaos1_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._create = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("create"));
        this._link = (String) args.get("link");
      }
    }
  
    public FoodAccessHouseholdInsertInput getCreate() { return this._create; }
    public String getLink() { return this._link; }
  }
  public static class GupsyClinicInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public GupsyClinicInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class NutritionixAttributeQueryInput {
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<NutritionixAttributeQueryInput> _AND;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Iterable<NutritionixAttributeQueryInput> _OR;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_lt;
    private Object _CAOS2_partitionKey_lte;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_ne;
    private Object _CAOS2_partitionKey_gt;
  
    public NutritionixAttributeQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(NutritionixAttributeQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(NutritionixAttributeQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
      }
    }
  
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<NutritionixAttributeQueryInput> getAnd() { return this._AND; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Iterable<NutritionixAttributeQueryInput> getOr() { return this._OR; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
  }
  public enum AlphaCaroteneSortByInput {
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC"),
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC");
    
    public final String label;
     
    AlphaCaroteneSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, AlphaCaroteneSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (AlphaCaroteneSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static AlphaCaroteneSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class FoodAccessHouseholdCaos1_PartitionKeyRelationInput {
    private FoodAccessHouseholdInsertInput _create;
    private String _link;
  
    public FoodAccessHouseholdCaos1_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._create = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("create"));
        this._link = (String) args.get("link");
      }
    }
  
    public FoodAccessHouseholdInsertInput getCreate() { return this._create; }
    public String getLink() { return this._link; }
  }
  
  public static class AlphaCaroteneInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public AlphaCaroteneInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class CholineUpdateInput {
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public CholineUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public static class FoodDrinkMeasurementUpdateInput {
    private FoodDrinkMeasurementCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public FoodDrinkMeasurementUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = new FoodDrinkMeasurementCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public FoodDrinkMeasurementCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public static class BetaCaroteneUpdateInput {
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
    private String _CAOS1_partitionKey;
  
    public BetaCaroteneUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
      }
    }
  
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
  }
  public static class NutritionixAttributeUpdateInput {
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public NutritionixAttributeUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public enum FoodDrinkMeasurementSortByInput {
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC"),
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC");
    
    public final String label;
     
    FoodDrinkMeasurementSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, FoodDrinkMeasurementSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (FoodDrinkMeasurementSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static FoodDrinkMeasurementSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class MutationDeleteManyAlphaCarotenesArgs {
    private AlphaCaroteneQueryInput _query;
  
    public MutationDeleteManyAlphaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyBetaCarotenesArgs {
    private BetaCaroteneQueryInput _query;
  
    public MutationDeleteManyBetaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyCholesterolsArgs {
    private CholesterolQueryInput _query;
  
    public MutationDeleteManyCholesterolsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyCholinesArgs {
    private CholineQueryInput _query;
  
    public MutationDeleteManyCholinesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyFoodAccessHouseholdsArgs {
    private FoodAccessHouseholdQueryInput _query;
  
    public MutationDeleteManyFoodAccessHouseholdsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyFoodDrinkMeasurementsArgs {
    private FoodDrinkMeasurementQueryInput _query;
  
    public MutationDeleteManyFoodDrinkMeasurementsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyFriedFoodsMortalitiesArgs {
    private FriedFoodsMortalityQueryInput _query;
  
    public MutationDeleteManyFriedFoodsMortalitiesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyGupsyClinicsArgs {
    private GupsyClinicQueryInput _query;
  
    public MutationDeleteManyGupsyClinicsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyGupsyHospitalsArgs {
    private GupsyHospitalQueryInput _query;
  
    public MutationDeleteManyGupsyHospitalsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyMcDataArgs {
    private McDatumQueryInput _query;
  
    public MutationDeleteManyMcDataArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyMentalHealthFaqsArgs {
    private MentalHealthFaqQueryInput _query;
  
    public MutationDeleteManyMentalHealthFaqsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyNutritionixAttributesArgs {
    private NutritionixAttributeQueryInput _query;
  
    public MutationDeleteManyNutritionixAttributesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyNutritionixGroceryListsArgs {
    private NutritionixGroceryListQueryInput _query;
  
    public MutationDeleteManyNutritionixGroceryListsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteManyYaleFoodAddictionScalesArgs {
    private YaleFoodAddictionScaleQueryInput _query;
  
    public MutationDeleteManyYaleFoodAddictionScalesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneAlphaCaroteneArgs {
    private AlphaCaroteneQueryInput _query;
  
    public MutationDeleteOneAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneBetaCaroteneArgs {
    private BetaCaroteneQueryInput _query;
  
    public MutationDeleteOneBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneCholesterolArgs {
    private CholesterolQueryInput _query;
  
    public MutationDeleteOneCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneCholineArgs {
    private CholineQueryInput _query;
  
    public MutationDeleteOneCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneFoodAccessHouseholdArgs {
    private FoodAccessHouseholdQueryInput _query;
  
    public MutationDeleteOneFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementQueryInput _query;
  
    public MutationDeleteOneFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneFriedFoodsMortalityArgs {
    private FriedFoodsMortalityQueryInput _query;
  
    public MutationDeleteOneFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneGupsyClinicArgs {
    private GupsyClinicQueryInput _query;
  
    public MutationDeleteOneGupsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneGupsyHospitalArgs {
    private GupsyHospitalQueryInput _query;
  
    public MutationDeleteOneGupsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneMcDatumArgs {
    private McDatumQueryInput _query;
  
    public MutationDeleteOneMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneMentalHealthFaqArgs {
    private MentalHealthFaqQueryInput _query;
  
    public MutationDeleteOneMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneNutritionixAttributeArgs {
    private NutritionixAttributeQueryInput _query;
  
    public MutationDeleteOneNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneNutritionixGroceryListArgs {
    private NutritionixGroceryListQueryInput _query;
  
    public MutationDeleteOneNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
  }
  public static class MutationDeleteOneYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleQueryInput _query;
  
    public MutationDeleteOneYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
  }
  public static class MutationInsertManyAlphaCarotenesArgs {
    private Iterable<AlphaCaroteneInsertInput> _data;
  
    public MutationInsertManyAlphaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(AlphaCaroteneInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<AlphaCaroteneInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyBetaCarotenesArgs {
    private Iterable<BetaCaroteneInsertInput> _data;
  
    public MutationInsertManyBetaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(BetaCaroteneInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<BetaCaroteneInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyCholesterolsArgs {
    private Iterable<CholesterolInsertInput> _data;
  
    public MutationInsertManyCholesterolsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(CholesterolInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<CholesterolInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyCholinesArgs {
    private Iterable<CholineInsertInput> _data;
  
    public MutationInsertManyCholinesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(CholineInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<CholineInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyFoodAccessHouseholdsArgs {
    private Iterable<FoodAccessHouseholdInsertInput> _data;
  
    public MutationInsertManyFoodAccessHouseholdsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(FoodAccessHouseholdInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<FoodAccessHouseholdInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyFoodDrinkMeasurementsArgs {
    private Iterable<FoodDrinkMeasurementInsertInput> _data;
  
    public MutationInsertManyFoodDrinkMeasurementsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(FoodDrinkMeasurementInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<FoodDrinkMeasurementInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyFriedFoodsMortalitiesArgs {
    private Iterable<FriedFoodsMortalityInsertInput> _data;
  
    public MutationInsertManyFriedFoodsMortalitiesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(FriedFoodsMortalityInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<FriedFoodsMortalityInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyGupsyClinicsArgs {
    private Iterable<GupsyClinicInsertInput> _data;
  
    public MutationInsertManyGupsyClinicsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(GupsyClinicInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<GupsyClinicInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyGupsyHospitalsArgs {
    private Iterable<GupsyHospitalInsertInput> _data;
  
    public MutationInsertManyGupsyHospitalsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(GupsyHospitalInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<GupsyHospitalInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyMcDataArgs {
    private Iterable<McDatumInsertInput> _data;
  
    public MutationInsertManyMcDataArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(McDatumInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<McDatumInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyMentalHealthFaqsArgs {
    private Iterable<MentalHealthFaqInsertInput> _data;
  
    public MutationInsertManyMentalHealthFaqsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(MentalHealthFaqInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<MentalHealthFaqInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyNutritionixAttributesArgs {
    private Iterable<NutritionixAttributeInsertInput> _data;
  
    public MutationInsertManyNutritionixAttributesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(NutritionixAttributeInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<NutritionixAttributeInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyNutritionixGroceryListsArgs {
    private Iterable<NutritionixGroceryListInsertInput> _data;
  
    public MutationInsertManyNutritionixGroceryListsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(NutritionixGroceryListInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<NutritionixGroceryListInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertManyYaleFoodAddictionScalesArgs {
    private Iterable<YaleFoodAddictionScaleInsertInput> _data;
  
    public MutationInsertManyYaleFoodAddictionScalesArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("data") != null) {
          this._data = ((List<Map<String, Object>>) args.get("data")).stream().map(YaleFoodAddictionScaleInsertInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<YaleFoodAddictionScaleInsertInput> getData() { return this._data; }
  }
  public static class MutationInsertOneAlphaCaroteneArgs {
    private AlphaCaroteneInsertInput _data;
  
    public MutationInsertOneAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new AlphaCaroteneInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public AlphaCaroteneInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneBetaCaroteneArgs {
    private BetaCaroteneInsertInput _data;
  
    public MutationInsertOneBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new BetaCaroteneInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public BetaCaroteneInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneCholesterolArgs {
    private CholesterolInsertInput _data;
  
    public MutationInsertOneCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new CholesterolInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholesterolInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneCholineArgs {
    private CholineInsertInput _data;
  
    public MutationInsertOneCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new CholineInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholineInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneFoodAccessHouseholdArgs {
    private FoodAccessHouseholdInsertInput _data;
  
    public MutationInsertOneFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FoodAccessHouseholdInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementInsertInput _data;
  
    public MutationInsertOneFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new FoodDrinkMeasurementInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FoodDrinkMeasurementInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneFriedFoodsMortalityArgs {
    private FriedFoodsMortalityInsertInput _data;
  
    public MutationInsertOneFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FriedFoodsMortalityInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneGupsyClinicArgs {
    private GupsyClinicInsertInput _data;
  
    public MutationInsertOneGupsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new GupsyClinicInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyClinicInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneGupsyHospitalArgs {
    private GupsyHospitalInsertInput _data;
  
    public MutationInsertOneGupsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new GupsyHospitalInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyHospitalInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneMcDatumArgs {
    private McDatumInsertInput _data;
  
    public MutationInsertOneMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new McDatumInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public McDatumInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneMentalHealthFaqArgs {
    private MentalHealthFaqInsertInput _data;
  
    public MutationInsertOneMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new MentalHealthFaqInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public MentalHealthFaqInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneNutritionixAttributeArgs {
    private NutritionixAttributeInsertInput _data;
  
    public MutationInsertOneNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new NutritionixAttributeInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixAttributeInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneNutritionixGroceryListArgs {
    private NutritionixGroceryListInsertInput _data;
  
    public MutationInsertOneNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new NutritionixGroceryListInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixGroceryListInsertInput getData() { return this._data; }
  }
  public static class MutationInsertOneYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleInsertInput _data;
  
    public MutationInsertOneYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new YaleFoodAddictionScaleInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public YaleFoodAddictionScaleInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneAlphaCaroteneArgs {
    private AlphaCaroteneQueryInput _query;
    private AlphaCaroteneInsertInput _data;
  
    public MutationReplaceOneAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._data = new AlphaCaroteneInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
    public AlphaCaroteneInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneBetaCaroteneArgs {
    private BetaCaroteneInsertInput _data;
    private BetaCaroteneQueryInput _query;
  
    public MutationReplaceOneBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new BetaCaroteneInsertInput((Map<String, Object>) args.get("data"));
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public BetaCaroteneInsertInput getData() { return this._data; }
    public BetaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationReplaceOneCholesterolArgs {
    private CholesterolQueryInput _query;
    private CholesterolInsertInput _data;
  
    public MutationReplaceOneCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
        this._data = new CholesterolInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
    public CholesterolInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneCholineArgs {
    private CholineQueryInput _query;
    private CholineInsertInput _data;
  
    public MutationReplaceOneCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
        this._data = new CholineInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
    public CholineInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneFoodAccessHouseholdArgs {
    private FoodAccessHouseholdQueryInput _query;
    private FoodAccessHouseholdInsertInput _data;
  
    public MutationReplaceOneFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
        this._data = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
    public FoodAccessHouseholdInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementInsertInput _data;
    private FoodDrinkMeasurementQueryInput _query;
  
    public MutationReplaceOneFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new FoodDrinkMeasurementInsertInput((Map<String, Object>) args.get("data"));
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodDrinkMeasurementInsertInput getData() { return this._data; }
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
  }
  public static class MutationReplaceOneFriedFoodsMortalityArgs {
    private FriedFoodsMortalityQueryInput _query;
    private FriedFoodsMortalityInsertInput _data;
  
    public MutationReplaceOneFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
        this._data = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
    public FriedFoodsMortalityInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneGupsyClinicArgs {
    private GupsyClinicQueryInput _query;
    private GupsyClinicInsertInput _data;
  
    public MutationReplaceOneGupsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
        this._data = new GupsyClinicInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
    public GupsyClinicInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneGupsyHospitalArgs {
    private GupsyHospitalQueryInput _query;
    private GupsyHospitalInsertInput _data;
  
    public MutationReplaceOneGupsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
        this._data = new GupsyHospitalInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
    public GupsyHospitalInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneMcDatumArgs {
    private McDatumQueryInput _query;
    private McDatumInsertInput _data;
  
    public MutationReplaceOneMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
        this._data = new McDatumInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
    public McDatumInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneMentalHealthFaqArgs {
    private MentalHealthFaqInsertInput _data;
    private MentalHealthFaqQueryInput _query;
  
    public MutationReplaceOneMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._data = new MentalHealthFaqInsertInput((Map<String, Object>) args.get("data"));
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public MentalHealthFaqInsertInput getData() { return this._data; }
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
  }
  public static class MutationReplaceOneNutritionixAttributeArgs {
    private NutritionixAttributeQueryInput _query;
    private NutritionixAttributeInsertInput _data;
  
    public MutationReplaceOneNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
        this._data = new NutritionixAttributeInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
    public NutritionixAttributeInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneNutritionixGroceryListArgs {
    private NutritionixGroceryListQueryInput _query;
    private NutritionixGroceryListInsertInput _data;
  
    public MutationReplaceOneNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
        this._data = new NutritionixGroceryListInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
    public NutritionixGroceryListInsertInput getData() { return this._data; }
  }
  public static class MutationReplaceOneYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleQueryInput _query;
    private YaleFoodAddictionScaleInsertInput _data;
  
    public MutationReplaceOneYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
        this._data = new YaleFoodAddictionScaleInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
    public YaleFoodAddictionScaleInsertInput getData() { return this._data; }
  }
  public static class MutationUpdateManyAlphaCarotenesArgs {
    private AlphaCaroteneUpdateInput _set;
    private AlphaCaroteneQueryInput _query;
  
    public MutationUpdateManyAlphaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._set = new AlphaCaroteneUpdateInput((Map<String, Object>) args.get("set"));
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public AlphaCaroteneUpdateInput getSet() { return this._set; }
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationUpdateManyBetaCarotenesArgs {
    private BetaCaroteneQueryInput _query;
    private BetaCaroteneUpdateInput _set;
  
    public MutationUpdateManyBetaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._set = new BetaCaroteneUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
    public BetaCaroteneUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyCholesterolsArgs {
    private CholesterolQueryInput _query;
    private CholesterolUpdateInput _set;
  
    public MutationUpdateManyCholesterolsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
        this._set = new CholesterolUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
    public CholesterolUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyCholinesArgs {
    private CholineQueryInput _query;
    private CholineUpdateInput _set;
  
    public MutationUpdateManyCholinesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
        this._set = new CholineUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
    public CholineUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyFoodAccessHouseholdsArgs {
    private FoodAccessHouseholdQueryInput _query;
    private FoodAccessHouseholdUpdateInput _set;
  
    public MutationUpdateManyFoodAccessHouseholdsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FoodAccessHouseholdUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
    public FoodAccessHouseholdUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyFoodDrinkMeasurementsArgs {
    private FoodDrinkMeasurementQueryInput _query;
    private FoodDrinkMeasurementUpdateInput _set;
  
    public MutationUpdateManyFoodDrinkMeasurementsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FoodDrinkMeasurementUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
    public FoodDrinkMeasurementUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyFriedFoodsMortalitiesArgs {
    private FriedFoodsMortalityQueryInput _query;
    private FriedFoodsMortalityUpdateInput _set;
  
    public MutationUpdateManyFriedFoodsMortalitiesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FriedFoodsMortalityUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
    public FriedFoodsMortalityUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyGupsyClinicsArgs {
    private GupsyClinicQueryInput _query;
    private GupsyClinicUpdateInput _set;
  
    public MutationUpdateManyGupsyClinicsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
        this._set = new GupsyClinicUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
    public GupsyClinicUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyGupsyHospitalsArgs {
    private GupsyHospitalQueryInput _query;
    private GupsyHospitalUpdateInput _set;
  
    public MutationUpdateManyGupsyHospitalsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
        this._set = new GupsyHospitalUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
    public GupsyHospitalUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyMcDataArgs {
    private McDatumQueryInput _query;
    private McDatumUpdateInput _set;
  
    public MutationUpdateManyMcDataArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
        this._set = new McDatumUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
    public McDatumUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyMentalHealthFaqsArgs {
    private MentalHealthFaqQueryInput _query;
    private MentalHealthFaqUpdateInput _set;
  
    public MutationUpdateManyMentalHealthFaqsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
        this._set = new MentalHealthFaqUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
    public MentalHealthFaqUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyNutritionixAttributesArgs {
    private NutritionixAttributeQueryInput _query;
    private NutritionixAttributeUpdateInput _set;
  
    public MutationUpdateManyNutritionixAttributesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
        this._set = new NutritionixAttributeUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
    public NutritionixAttributeUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyNutritionixGroceryListsArgs {
    private NutritionixGroceryListQueryInput _query;
    private NutritionixGroceryListUpdateInput _set;
  
    public MutationUpdateManyNutritionixGroceryListsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
        this._set = new NutritionixGroceryListUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
    public NutritionixGroceryListUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateManyYaleFoodAddictionScalesArgs {
    private YaleFoodAddictionScaleQueryInput _query;
    private YaleFoodAddictionScaleUpdateInput _set;
  
    public MutationUpdateManyYaleFoodAddictionScalesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
        this._set = new YaleFoodAddictionScaleUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
    public YaleFoodAddictionScaleUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneAlphaCaroteneArgs {
    private AlphaCaroteneQueryInput _query;
    private AlphaCaroteneUpdateInput _set;
  
    public MutationUpdateOneAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._set = new AlphaCaroteneUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
    public AlphaCaroteneUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneBetaCaroteneArgs {
    private BetaCaroteneUpdateInput _set;
    private BetaCaroteneQueryInput _query;
  
    public MutationUpdateOneBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._set = new BetaCaroteneUpdateInput((Map<String, Object>) args.get("set"));
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public BetaCaroteneUpdateInput getSet() { return this._set; }
    public BetaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class MutationUpdateOneCholesterolArgs {
    private CholesterolUpdateInput _set;
    private CholesterolQueryInput _query;
  
    public MutationUpdateOneCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._set = new CholesterolUpdateInput((Map<String, Object>) args.get("set"));
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholesterolUpdateInput getSet() { return this._set; }
    public CholesterolQueryInput getQuery() { return this._query; }
  }
  public static class MutationUpdateOneCholineArgs {
    private CholineQueryInput _query;
    private CholineUpdateInput _set;
  
    public MutationUpdateOneCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
        this._set = new CholineUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
    public CholineUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneFoodAccessHouseholdArgs {
    private FoodAccessHouseholdQueryInput _query;
    private FoodAccessHouseholdUpdateInput _set;
  
    public MutationUpdateOneFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FoodAccessHouseholdUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
    public FoodAccessHouseholdUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementQueryInput _query;
    private FoodDrinkMeasurementUpdateInput _set;
  
    public MutationUpdateOneFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FoodDrinkMeasurementUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
    public FoodDrinkMeasurementUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneFriedFoodsMortalityArgs {
    private FriedFoodsMortalityQueryInput _query;
    private FriedFoodsMortalityUpdateInput _set;
  
    public MutationUpdateOneFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
        this._set = new FriedFoodsMortalityUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
    public FriedFoodsMortalityUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneGupsyClinicArgs {
    private GupsyClinicQueryInput _query;
    private GupsyClinicUpdateInput _set;
  
    public MutationUpdateOneGupsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
        this._set = new GupsyClinicUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
    public GupsyClinicUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneGupsyHospitalArgs {
    private GupsyHospitalQueryInput _query;
    private GupsyHospitalUpdateInput _set;
  
    public MutationUpdateOneGupsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
        this._set = new GupsyHospitalUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
    public GupsyHospitalUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneMcDatumArgs {
    private McDatumUpdateInput _set;
    private McDatumQueryInput _query;
  
    public MutationUpdateOneMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._set = new McDatumUpdateInput((Map<String, Object>) args.get("set"));
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public McDatumUpdateInput getSet() { return this._set; }
    public McDatumQueryInput getQuery() { return this._query; }
  }
  public static class MutationUpdateOneMentalHealthFaqArgs {
    private MentalHealthFaqQueryInput _query;
    private MentalHealthFaqUpdateInput _set;
  
    public MutationUpdateOneMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
        this._set = new MentalHealthFaqUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
    public MentalHealthFaqUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneNutritionixAttributeArgs {
    private NutritionixAttributeQueryInput _query;
    private NutritionixAttributeUpdateInput _set;
  
    public MutationUpdateOneNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
        this._set = new NutritionixAttributeUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
    public NutritionixAttributeUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneNutritionixGroceryListArgs {
    private NutritionixGroceryListQueryInput _query;
    private NutritionixGroceryListUpdateInput _set;
  
    public MutationUpdateOneNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
        this._set = new NutritionixGroceryListUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
    public NutritionixGroceryListUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpdateOneYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleQueryInput _query;
    private YaleFoodAddictionScaleUpdateInput _set;
  
    public MutationUpdateOneYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
        this._set = new YaleFoodAddictionScaleUpdateInput((Map<String, Object>) args.get("set"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
    public YaleFoodAddictionScaleUpdateInput getSet() { return this._set; }
  }
  public static class MutationUpsertOneAlphaCaroteneArgs {
    private AlphaCaroteneQueryInput _query;
    private AlphaCaroteneInsertInput _data;
  
    public MutationUpsertOneAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._data = new AlphaCaroteneInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
    public AlphaCaroteneInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneBetaCaroteneArgs {
    private BetaCaroteneQueryInput _query;
    private BetaCaroteneInsertInput _data;
  
    public MutationUpsertOneBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._data = new BetaCaroteneInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
    public BetaCaroteneInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneCholesterolArgs {
    private CholesterolQueryInput _query;
    private CholesterolInsertInput _data;
  
    public MutationUpsertOneCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
        this._data = new CholesterolInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
    public CholesterolInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneCholineArgs {
    private CholineQueryInput _query;
    private CholineInsertInput _data;
  
    public MutationUpsertOneCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
        this._data = new CholineInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
    public CholineInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneFoodAccessHouseholdArgs {
    private FoodAccessHouseholdQueryInput _query;
    private FoodAccessHouseholdInsertInput _data;
  
    public MutationUpsertOneFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
        this._data = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
    public FoodAccessHouseholdInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementQueryInput _query;
    private FoodDrinkMeasurementInsertInput _data;
  
    public MutationUpsertOneFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
        this._data = new FoodDrinkMeasurementInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
    public FoodDrinkMeasurementInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneFriedFoodsMortalityArgs {
    private FriedFoodsMortalityQueryInput _query;
    private FriedFoodsMortalityInsertInput _data;
  
    public MutationUpsertOneFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
        this._data = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
    public FriedFoodsMortalityInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneGupsyClinicArgs {
    private GupsyClinicQueryInput _query;
    private GupsyClinicInsertInput _data;
  
    public MutationUpsertOneGupsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
        this._data = new GupsyClinicInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
    public GupsyClinicInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneGupsyHospitalArgs {
    private GupsyHospitalQueryInput _query;
    private GupsyHospitalInsertInput _data;
  
    public MutationUpsertOneGupsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
        this._data = new GupsyHospitalInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
    public GupsyHospitalInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneMcDatumArgs {
    private McDatumQueryInput _query;
    private McDatumInsertInput _data;
  
    public MutationUpsertOneMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
        this._data = new McDatumInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
    public McDatumInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneMentalHealthFaqArgs {
    private MentalHealthFaqQueryInput _query;
    private MentalHealthFaqInsertInput _data;
  
    public MutationUpsertOneMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
        this._data = new MentalHealthFaqInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
    public MentalHealthFaqInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneNutritionixAttributeArgs {
    private NutritionixAttributeQueryInput _query;
    private NutritionixAttributeInsertInput _data;
  
    public MutationUpsertOneNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
        this._data = new NutritionixAttributeInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
    public NutritionixAttributeInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneNutritionixGroceryListArgs {
    private NutritionixGroceryListQueryInput _query;
    private NutritionixGroceryListInsertInput _data;
  
    public MutationUpsertOneNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
        this._data = new NutritionixGroceryListInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
    public NutritionixGroceryListInsertInput getData() { return this._data; }
  }
  public static class MutationUpsertOneYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleQueryInput _query;
    private YaleFoodAddictionScaleInsertInput _data;
  
    public MutationUpsertOneYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
        this._data = new YaleFoodAddictionScaleInsertInput((Map<String, Object>) args.get("data"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
    public YaleFoodAddictionScaleInsertInput getData() { return this._data; }
  }
  public static class NutritionixAttributeInsertInput {
    private Object _CAOS2_partitionKey;
  
    public NutritionixAttributeInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  
  public static class FriedFoodsMortalityCaos2_PartitionKeyRelationInput {
    private Object _link;
    private FriedFoodsMortalityInsertInput _create;
  
    public FriedFoodsMortalityCaos2_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._link = (Object) args.get("link");
        this._create = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("create"));
      }
    }
  
    public Object getLink() { return this._link; }
    public FriedFoodsMortalityInsertInput getCreate() { return this._create; }
  }
  public static class GupsyHospitalQueryInput {
    private String _CAOS1_partitionKey_gt;
    private String _CAOS1_partitionKey_ne;
    private Boolean _CAOS2_partitionKey_exists;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey;
    private String _CAOS1_partitionKey_gte;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private FriedFoodsMortalityQueryInput _CAOS2_partitionKey;
    private String _CAOS1_partitionKey_lte;
    private String _CAOS1_partitionKey_lt;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Iterable<GupsyHospitalQueryInput> _AND;
    private Iterable<GupsyHospitalQueryInput> _OR;
  
    public GupsyHospitalQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS2_partitionKey = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(GupsyHospitalQueryInput::new).collect(Collectors.toList());
        }
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(GupsyHospitalQueryInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public FriedFoodsMortalityQueryInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Iterable<GupsyHospitalQueryInput> getAnd() { return this._AND; }
    public Iterable<GupsyHospitalQueryInput> getOr() { return this._OR; }
  }
  public static class YaleFoodAddictionScaleUpdateInput {
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
    private String _CAOS1_partitionKey;
  
    public YaleFoodAddictionScaleUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
      }
    }
  
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
  }
  public static class McDatumUpdateInput {
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private McDatumCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public McDatumUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = new McDatumCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public McDatumCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public enum CholesterolSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    CholesterolSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, CholesterolSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (CholesterolSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static CholesterolSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public enum NutritionixGroceryListSortByInput {
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC"),
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC");
    
    public final String label;
     
    NutritionixGroceryListSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, NutritionixGroceryListSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (NutritionixGroceryListSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static NutritionixGroceryListSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public enum YaleFoodAddictionScaleSortByInput {
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC"),
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC");
    
    public final String label;
     
    YaleFoodAddictionScaleSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, YaleFoodAddictionScaleSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (YaleFoodAddictionScaleSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static YaleFoodAddictionScaleSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public enum McDatumSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    McDatumSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, McDatumSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (McDatumSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static McDatumSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class NutritionixGroceryListInsertInput {
    private NutritionixGroceryListCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public NutritionixGroceryListInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = new NutritionixGroceryListCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public NutritionixGroceryListCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  
  public static class FriedFoodsMortalityUpdateInput {
    private FriedFoodsMortalityCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public FriedFoodsMortalityUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = new FriedFoodsMortalityCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public FriedFoodsMortalityCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public static class QueryCaosArgs {
    private Integer _input;
  
    public QueryCaosArgs(Map<String, Object> args) {
      if (args != null) {
        this._input = (Integer) args.get("input");
      }
    }
  
    public Integer getInput() { return this._input; }
  }
  public static class QueryCaos1Args {
    private Double _input;
  
    public QueryCaos1Args(Map<String, Object> args) {
      if (args != null) {
        this._input = (Double) args.get("input");
      }
    }
  
    public Double getInput() { return this._input; }
  }
  public static class QueryCaos2Args {
    private Object _input;
  
    public QueryCaos2Args(Map<String, Object> args) {
      if (args != null) {
        this._input = (Object) args.get("input");
      }
    }
  
    public Object getInput() { return this._input; }
  }
  public static class QueryCaos3Args {
    private Object _input;
  
    public QueryCaos3Args(Map<String, Object> args) {
      if (args != null) {
        this._input = (Object) args.get("input");
      }
    }
  
    public Object getInput() { return this._input; }
  }
  public static class QueryCaos4Args {
    private String _input;
  
    public QueryCaos4Args(Map<String, Object> args) {
      if (args != null) {
        this._input = (String) args.get("input");
      }
    }
  
    public String getInput() { return this._input; }
  }
  public static class QueryCaos5Args {
    private Boolean _input;
  
    public QueryCaos5Args(Map<String, Object> args) {
      if (args != null) {
        this._input = (Boolean) args.get("input");
      }
    }
  
    public Boolean getInput() { return this._input; }
  }
  public static class QueryAlphaCaroteneArgs {
    private AlphaCaroteneQueryInput _query;
  
    public QueryAlphaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class QueryAlphaCarotenesArgs {
    private AlphaCaroteneQueryInput _query;
    private Integer _limit;
    private AlphaCaroteneSortByInput _sortBy;
  
    public QueryAlphaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new AlphaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof AlphaCaroteneSortByInput) {
          this._sortBy = (AlphaCaroteneSortByInput) args.get("sortBy");
        } else {
          this._sortBy = AlphaCaroteneSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public AlphaCaroteneQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public AlphaCaroteneSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryBetaCaroteneArgs {
    private BetaCaroteneQueryInput _query;
  
    public QueryBetaCaroteneArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
  }
  public static class QueryBetaCarotenesArgs {
    private BetaCaroteneQueryInput _query;
    private Integer _limit;
    private BetaCaroteneSortByInput _sortBy;
  
    public QueryBetaCarotenesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new BetaCaroteneQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof BetaCaroteneSortByInput) {
          this._sortBy = (BetaCaroteneSortByInput) args.get("sortBy");
        } else {
          this._sortBy = BetaCaroteneSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public BetaCaroteneQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public BetaCaroteneSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryCholesterolArgs {
    private CholesterolQueryInput _query;
  
    public QueryCholesterolArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
  }
  public static class QueryCholesterolsArgs {
    private CholesterolQueryInput _query;
    private Integer _limit;
    private CholesterolSortByInput _sortBy;
  
    public QueryCholesterolsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholesterolQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof CholesterolSortByInput) {
          this._sortBy = (CholesterolSortByInput) args.get("sortBy");
        } else {
          this._sortBy = CholesterolSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public CholesterolQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public CholesterolSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryCholineArgs {
    private CholineQueryInput _query;
  
    public QueryCholineArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
  }
  public static class QueryCholinesArgs {
    private CholineQueryInput _query;
    private Integer _limit;
    private CholineSortByInput _sortBy;
  
    public QueryCholinesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new CholineQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof CholineSortByInput) {
          this._sortBy = (CholineSortByInput) args.get("sortBy");
        } else {
          this._sortBy = CholineSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public CholineQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public CholineSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryFoodAccessHouseholdArgs {
    private FoodAccessHouseholdQueryInput _query;
  
    public QueryFoodAccessHouseholdArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
  }
  public static class QueryFoodAccessHouseholdsArgs {
    private FoodAccessHouseholdQueryInput _query;
    private Integer _limit;
    private FoodAccessHouseholdSortByInput _sortBy;
  
    public QueryFoodAccessHouseholdsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof FoodAccessHouseholdSortByInput) {
          this._sortBy = (FoodAccessHouseholdSortByInput) args.get("sortBy");
        } else {
          this._sortBy = FoodAccessHouseholdSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public FoodAccessHouseholdQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public FoodAccessHouseholdSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryFoodDrinkMeasurementArgs {
    private FoodDrinkMeasurementQueryInput _query;
  
    public QueryFoodDrinkMeasurementArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
  }
  public static class QueryFoodDrinkMeasurementsArgs {
    private FoodDrinkMeasurementSortByInput _sortBy;
    private FoodDrinkMeasurementQueryInput _query;
    private Integer _limit;
  
    public QueryFoodDrinkMeasurementsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("sortBy") instanceof FoodDrinkMeasurementSortByInput) {
          this._sortBy = (FoodDrinkMeasurementSortByInput) args.get("sortBy");
        } else {
          this._sortBy = FoodDrinkMeasurementSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
        this._query = new FoodDrinkMeasurementQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
      }
    }
  
    public FoodDrinkMeasurementSortByInput getSortBy() { return this._sortBy; }
    public FoodDrinkMeasurementQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
  }
  public static class QueryFriedFoodsMortalitiesArgs {
    private FriedFoodsMortalityQueryInput _query;
    private Integer _limit;
    private FriedFoodsMortalitySortByInput _sortBy;
  
    public QueryFriedFoodsMortalitiesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof FriedFoodsMortalitySortByInput) {
          this._sortBy = (FriedFoodsMortalitySortByInput) args.get("sortBy");
        } else {
          this._sortBy = FriedFoodsMortalitySortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public FriedFoodsMortalitySortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryFriedFoodsMortalityArgs {
    private FriedFoodsMortalityQueryInput _query;
  
    public QueryFriedFoodsMortalityArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public FriedFoodsMortalityQueryInput getQuery() { return this._query; }
  }
  public static class QueryGUpsyClinicArgs {
    private GupsyClinicQueryInput _query;
  
    public QueryGUpsyClinicArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
  }
  public static class QueryGUpsyClinicsArgs {
    private GupsyClinicQueryInput _query;
    private Integer _limit;
    private GupsyClinicSortByInput _sortBy;
  
    public QueryGUpsyClinicsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyClinicQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof GupsyClinicSortByInput) {
          this._sortBy = (GupsyClinicSortByInput) args.get("sortBy");
        } else {
          this._sortBy = GupsyClinicSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public GupsyClinicQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public GupsyClinicSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryGUpsyHospitalArgs {
    private GupsyHospitalQueryInput _query;
  
    public QueryGUpsyHospitalArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
  }
  public static class QueryGUpsyHospitalsArgs {
    private GupsyHospitalQueryInput _query;
    private Integer _limit;
    private GupsyHospitalSortByInput _sortBy;
  
    public QueryGUpsyHospitalsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new GupsyHospitalQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof GupsyHospitalSortByInput) {
          this._sortBy = (GupsyHospitalSortByInput) args.get("sortBy");
        } else {
          this._sortBy = GupsyHospitalSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public GupsyHospitalQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public GupsyHospitalSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryMcDataArgs {
    private McDatumQueryInput _query;
    private Integer _limit;
    private McDatumSortByInput _sortBy;
  
    public QueryMcDataArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof McDatumSortByInput) {
          this._sortBy = (McDatumSortByInput) args.get("sortBy");
        } else {
          this._sortBy = McDatumSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public McDatumSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryMcDatumArgs {
    private McDatumQueryInput _query;
  
    public QueryMcDatumArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new McDatumQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public McDatumQueryInput getQuery() { return this._query; }
  }
  public static class QueryMentalHealthFaqArgs {
    private MentalHealthFaqQueryInput _query;
  
    public QueryMentalHealthFaqArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
  }
  public static class QueryMentalHealthFaqsArgs {
    private MentalHealthFaqSortByInput _sortBy;
    private MentalHealthFaqQueryInput _query;
    private Integer _limit;
  
    public QueryMentalHealthFaqsArgs(Map<String, Object> args) {
      if (args != null) {
        if (args.get("sortBy") instanceof MentalHealthFaqSortByInput) {
          this._sortBy = (MentalHealthFaqSortByInput) args.get("sortBy");
        } else {
          this._sortBy = MentalHealthFaqSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
        this._query = new MentalHealthFaqQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
      }
    }
  
    public MentalHealthFaqSortByInput getSortBy() { return this._sortBy; }
    public MentalHealthFaqQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
  }
  public static class QueryNutritionixAttributeArgs {
    private NutritionixAttributeQueryInput _query;
  
    public QueryNutritionixAttributeArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
  }
  public static class QueryNutritionixAttributesArgs {
    private NutritionixAttributeQueryInput _query;
    private Integer _limit;
    private NutritionixAttributeSortByInput _sortBy;
  
    public QueryNutritionixAttributesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixAttributeQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof NutritionixAttributeSortByInput) {
          this._sortBy = (NutritionixAttributeSortByInput) args.get("sortBy");
        } else {
          this._sortBy = NutritionixAttributeSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public NutritionixAttributeQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public NutritionixAttributeSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryNutritionixGroceryListArgs {
    private NutritionixGroceryListQueryInput _query;
  
    public QueryNutritionixGroceryListArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
  }
  public static class QueryNutritionixGroceryListsArgs {
    private NutritionixGroceryListQueryInput _query;
    private Integer _limit;
    private NutritionixGroceryListSortByInput _sortBy;
  
    public QueryNutritionixGroceryListsArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new NutritionixGroceryListQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof NutritionixGroceryListSortByInput) {
          this._sortBy = (NutritionixGroceryListSortByInput) args.get("sortBy");
        } else {
          this._sortBy = NutritionixGroceryListSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public NutritionixGroceryListQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public NutritionixGroceryListSortByInput getSortBy() { return this._sortBy; }
  }
  public static class QueryYaleFoodAddictionScaleArgs {
    private YaleFoodAddictionScaleQueryInput _query;
  
    public QueryYaleFoodAddictionScaleArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
  }
  public static class QueryYaleFoodAddictionScalesArgs {
    private YaleFoodAddictionScaleQueryInput _query;
    private Integer _limit;
    private YaleFoodAddictionScaleSortByInput _sortBy;
  
    public QueryYaleFoodAddictionScalesArgs(Map<String, Object> args) {
      if (args != null) {
        this._query = new YaleFoodAddictionScaleQueryInput((Map<String, Object>) args.get("query"));
        this._limit = (Integer) args.get("limit");
        if (args.get("sortBy") instanceof YaleFoodAddictionScaleSortByInput) {
          this._sortBy = (YaleFoodAddictionScaleSortByInput) args.get("sortBy");
        } else {
          this._sortBy = YaleFoodAddictionScaleSortByInput.valueOfLabel((String) args.get("sortBy"));
        }
      }
    }
  
    public YaleFoodAddictionScaleQueryInput getQuery() { return this._query; }
    public Integer getLimit() { return this._limit; }
    public YaleFoodAddictionScaleSortByInput getSortBy() { return this._sortBy; }
  }
  
  public static class MentalHealthFaqUpdateInput {
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public MentalHealthFaqUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public static class FoodDrinkMeasurementCaos1_PartitionKeyRelationInput {
    private String _link;
    private FoodAccessHouseholdInsertInput _create;
  
    public FoodDrinkMeasurementCaos1_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._link = (String) args.get("link");
        this._create = new FoodAccessHouseholdInsertInput((Map<String, Object>) args.get("create"));
      }
    }
  
    public String getLink() { return this._link; }
    public FoodAccessHouseholdInsertInput getCreate() { return this._create; }
  }
  public enum FoodAccessHouseholdSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    FoodAccessHouseholdSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, FoodAccessHouseholdSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (FoodAccessHouseholdSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static FoodAccessHouseholdSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class NutritionixGroceryListQueryInput {
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Object _CAOS2_partitionKey_gt;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_lt;
    private FoodAccessHouseholdQueryInput _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey_ne;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<NutritionixGroceryListQueryInput> _OR;
    private Iterable<NutritionixGroceryListQueryInput> _AND;
    private Boolean _CAOS1_partitionKey_exists;
    private Object _CAOS2_partitionKey;
  
    public NutritionixGroceryListQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS1_partitionKey = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(NutritionixGroceryListQueryInput::new).collect(Collectors.toList());
        }
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(NutritionixGroceryListQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public FoodAccessHouseholdQueryInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<NutritionixGroceryListQueryInput> getOr() { return this._OR; }
    public Iterable<NutritionixGroceryListQueryInput> getAnd() { return this._AND; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class FoodAccessHouseholdQueryInput {
    private Object _CAOS2_partitionKey_gte;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private FoodAccessHouseholdQueryInput _CAOS1_partitionKey;
    private Iterable<FoodAccessHouseholdQueryInput> _AND;
    private Boolean _CAOS1_partitionKey_exists;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_ne;
    private Iterable<FoodAccessHouseholdQueryInput> _OR;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_lt;
    private Object _CAOS2_partitionKey_lte;
    private Object _CAOS2_partitionKey_gt;
  
    public FoodAccessHouseholdQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS1_partitionKey = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(FoodAccessHouseholdQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(FoodAccessHouseholdQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
      }
    }
  
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public FoodAccessHouseholdQueryInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Iterable<FoodAccessHouseholdQueryInput> getAnd() { return this._AND; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Iterable<FoodAccessHouseholdQueryInput> getOr() { return this._OR; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
  }
  public static class GupsyClinicQueryInput {
    private Iterable<GupsyClinicQueryInput> _AND;
    private Boolean _CAOS1_partitionKey_exists;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Object _CAOS2_partitionKey;
    private Iterable<GupsyClinicQueryInput> _OR;
    private String _CAOS1_partitionKey;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Object _CAOS2_partitionKey_lt;
    private String _CAOS1_partitionKey_lte;
    private String _CAOS1_partitionKey_lt;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_ne;
    private Object _CAOS2_partitionKey_gt;
    private String _CAOS1_partitionKey_ne;
    private String _CAOS1_partitionKey_gt;
    private Boolean _CAOS2_partitionKey_exists;
    private String _CAOS1_partitionKey_gte;
  
    public GupsyClinicQueryInput(Map<String, Object> args) {
      if (args != null) {
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(GupsyClinicQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(GupsyClinicQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
      }
    }
  
    public Iterable<GupsyClinicQueryInput> getAnd() { return this._AND; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Iterable<GupsyClinicQueryInput> getOr() { return this._OR; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
  }
  
  public enum BetaCaroteneSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    BetaCaroteneSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, BetaCaroteneSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (BetaCaroteneSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static BetaCaroteneSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class GupsyClinicUpdateInput {
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public GupsyClinicUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public enum MentalHealthFaqSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    MentalHealthFaqSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, MentalHealthFaqSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (MentalHealthFaqSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static MentalHealthFaqSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class MentalHealthFaqQueryInput {
    private Object _CAOS2_partitionKey_lt;
    private Iterable<MentalHealthFaqQueryInput> _AND;
    private Boolean _CAOS1_partitionKey_exists;
    private Iterable<MentalHealthFaqQueryInput> _OR;
    private Object _CAOS2_partitionKey_gt;
    private String _CAOS1_partitionKey_lt;
    private String _CAOS1_partitionKey_lte;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_exists;
    private String _CAOS1_partitionKey_gt;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Object _CAOS2_partitionKey_gte;
    private String _CAOS1_partitionKey_gte;
    private Object _CAOS2_partitionKey_ne;
    private String _CAOS1_partitionKey;
    private String _CAOS1_partitionKey_ne;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Iterable<String> _CAOS1_partitionKey_nin;
  
    public MentalHealthFaqQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(MentalHealthFaqQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(MentalHealthFaqQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
      }
    }
  
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Iterable<MentalHealthFaqQueryInput> getAnd() { return this._AND; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Iterable<MentalHealthFaqQueryInput> getOr() { return this._OR; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
  }
  
  public static class FoodAccessHouseholdUpdateInput {
    private Boolean _CAOS2_partitionKey_unset;
    private FoodAccessHouseholdCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
  
    public FoodAccessHouseholdUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = new FoodAccessHouseholdCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public FoodAccessHouseholdCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class GupsyHospitalCaos2_PartitionKeyRelationInput {
    private Object _link;
    private FriedFoodsMortalityInsertInput _create;
  
    public GupsyHospitalCaos2_PartitionKeyRelationInput(Map<String, Object> args) {
      if (args != null) {
        this._link = (Object) args.get("link");
        this._create = new FriedFoodsMortalityInsertInput((Map<String, Object>) args.get("create"));
      }
    }
  
    public Object getLink() { return this._link; }
    public FriedFoodsMortalityInsertInput getCreate() { return this._create; }
  }
  
  public static class AlphaCaroteneQueryInput {
    private String _CAOS1_partitionKey_lt;
    private String _CAOS1_partitionKey_lte;
    private String _CAOS1_partitionKey_ne;
    private String _CAOS1_partitionKey;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private String _CAOS1_partitionKey_gte;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_gt;
    private Iterable<AlphaCaroteneQueryInput> _OR;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Object _CAOS2_partitionKey_lte;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey_gt;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_ne;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_lt;
    private Iterable<AlphaCaroteneQueryInput> _AND;
  
    public AlphaCaroteneQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(AlphaCaroteneQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(AlphaCaroteneQueryInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Iterable<AlphaCaroteneQueryInput> getOr() { return this._OR; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Iterable<AlphaCaroteneQueryInput> getAnd() { return this._AND; }
  }
  public enum FriedFoodsMortalitySortByInput {
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    FriedFoodsMortalitySortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, FriedFoodsMortalitySortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (FriedFoodsMortalitySortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static FriedFoodsMortalitySortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class McDatumInsertInput {
    private String _CAOS1_partitionKey;
    private McDatumCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
  
    public McDatumInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = new McDatumCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public McDatumCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class NutritionixGroceryListUpdateInput {
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
    private NutritionixGroceryListCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
  
    public NutritionixGroceryListUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
        this._CAOS1_partitionKey = new NutritionixGroceryListCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
    public NutritionixGroceryListCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
  }
  
  public enum GupsyClinicSortByInput {
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC"),
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    GupsyClinicSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, GupsyClinicSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (GupsyClinicSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static GupsyClinicSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class CholineInsertInput {
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public CholineInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class CholineQueryInput {
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_gt;
    private Object _CAOS2_partitionKey_lt;
    private String _CAOS1_partitionKey_gte;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Iterable<CholineQueryInput> _AND;
    private Object _CAOS2_partitionKey_ne;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private String _CAOS1_partitionKey_lte;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey_ne;
    private String _CAOS1_partitionKey_gt;
    private String _CAOS1_partitionKey_lt;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_gte;
    private Iterable<CholineQueryInput> _OR;
    private String _CAOS1_partitionKey;
  
    public CholineQueryInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(CholineQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(CholineQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Iterable<CholineQueryInput> getAnd() { return this._AND; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Iterable<CholineQueryInput> getOr() { return this._OR; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
  }
  public enum GupsyHospitalSortByInput {
    Caos1PartitionkeyDesc("CAOS1_PARTITIONKEY_DESC"),
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC"),
    Caos1PartitionkeyAsc("CAOS1_PARTITIONKEY_ASC");
    
    public final String label;
     
    GupsyHospitalSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, GupsyHospitalSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (GupsyHospitalSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static GupsyHospitalSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  public static class FoodDrinkMeasurementInsertInput {
    private FoodDrinkMeasurementCaos1_PartitionKeyRelationInput _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey;
  
    public FoodDrinkMeasurementInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = new FoodDrinkMeasurementCaos1_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
      }
    }
  
    public FoodDrinkMeasurementCaos1_PartitionKeyRelationInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class FriedFoodsMortalityQueryInput {
    private Iterable<FriedFoodsMortalityQueryInput> _OR;
    private Iterable<FriedFoodsMortalityQueryInput> _AND;
    private FriedFoodsMortalityQueryInput _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_exists;
  
    public FriedFoodsMortalityQueryInput(Map<String, Object> args) {
      if (args != null) {
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(FriedFoodsMortalityQueryInput::new).collect(Collectors.toList());
        }
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(FriedFoodsMortalityQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey = new FriedFoodsMortalityQueryInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
      }
    }
  
    public Iterable<FriedFoodsMortalityQueryInput> getOr() { return this._OR; }
    public Iterable<FriedFoodsMortalityQueryInput> getAnd() { return this._AND; }
    public FriedFoodsMortalityQueryInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
  }
  
  public enum NutritionixAttributeSortByInput {
    Caos2PartitionkeyAsc("CAOS2_PARTITIONKEY_ASC"),
    Caos2PartitionkeyDesc("CAOS2_PARTITIONKEY_DESC");
    
    public final String label;
     
    NutritionixAttributeSortByInput(String label) {
      this.label = label;
    }
    
    private static final Map<String, NutritionixAttributeSortByInput> BY_LABEL = new HashMap<>();
      
    static {
        for (NutritionixAttributeSortByInput e : values()) {
            BY_LABEL.put(e.label, e);
        }
    }
    
    public static NutritionixAttributeSortByInput valueOfLabel(String label) {
      return BY_LABEL.get(label);
    }
  }
  
  
  public static class AlphaCaroteneUpdateInput {
    private String _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_unset;
    private Object _CAOS2_partitionKey;
    private Boolean _CAOS2_partitionKey_unset;
  
    public AlphaCaroteneUpdateInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS1_partitionKey_unset = (Boolean) args.get("CAOS1_partitionKey_unset");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_unset = (Boolean) args.get("CAOS2_partitionKey_unset");
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Unset() { return this._CAOS1_partitionKey_unset; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Boolean getCaos2_PartitionKey_Unset() { return this._CAOS2_partitionKey_unset; }
  }
  public static class CholesterolInsertInput {
    private Object _CAOS2_partitionKey;
    private String _CAOS1_partitionKey;
  
    public CholesterolInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
      }
    }
  
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
  }
  public static class CholesterolQueryInput {
    private Iterable<CholesterolQueryInput> _AND;
    private String _CAOS1_partitionKey_gt;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_gt;
    private Boolean _CAOS1_partitionKey_exists;
    private String _CAOS1_partitionKey_lte;
    private Iterable<CholesterolQueryInput> _OR;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Boolean _CAOS2_partitionKey_exists;
    private String _CAOS1_partitionKey_gte;
    private String _CAOS1_partitionKey;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_ne;
    private Iterable<String> _CAOS1_partitionKey_nin;
    private String _CAOS1_partitionKey_ne;
    private String _CAOS1_partitionKey_lt;
    private Object _CAOS2_partitionKey_lt;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<String> _CAOS1_partitionKey_in;
    private Object _CAOS2_partitionKey_lte;
  
    public CholesterolQueryInput(Map<String, Object> args) {
      if (args != null) {
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(CholesterolQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS1_partitionKey_gt = (String) args.get("CAOS1_partitionKey_gt");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS1_partitionKey_lte = (String) args.get("CAOS1_partitionKey_lte");
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(CholesterolQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS1_partitionKey_gte = (String) args.get("CAOS1_partitionKey_gte");
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS1_partitionKey_nin = (Iterable<String>) args.get("CAOS1_partitionKey_nin");
        this._CAOS1_partitionKey_ne = (String) args.get("CAOS1_partitionKey_ne");
        this._CAOS1_partitionKey_lt = (String) args.get("CAOS1_partitionKey_lt");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS1_partitionKey_in = (Iterable<String>) args.get("CAOS1_partitionKey_in");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
      }
    }
  
    public Iterable<CholesterolQueryInput> getAnd() { return this._AND; }
    public String getCaos1_PartitionKey_Gt() { return this._CAOS1_partitionKey_gt; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public String getCaos1_PartitionKey_Lte() { return this._CAOS1_partitionKey_lte; }
    public Iterable<CholesterolQueryInput> getOr() { return this._OR; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public String getCaos1_PartitionKey_Gte() { return this._CAOS1_partitionKey_gte; }
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Iterable<String> getCaos1_PartitionKey_Nin() { return this._CAOS1_partitionKey_nin; }
    public String getCaos1_PartitionKey_Ne() { return this._CAOS1_partitionKey_ne; }
    public String getCaos1_PartitionKey_Lt() { return this._CAOS1_partitionKey_lt; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<String> getCaos1_PartitionKey_In() { return this._CAOS1_partitionKey_in; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
  }
  
  public static class FriedFoodsMortalityInsertInput {
    private FriedFoodsMortalityCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
  
    public FriedFoodsMortalityInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS2_partitionKey = new FriedFoodsMortalityCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
      }
    }
  
    public FriedFoodsMortalityCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class GupsyHospitalInsertInput {
    private String _CAOS1_partitionKey;
    private GupsyHospitalCaos2_PartitionKeyRelationInput _CAOS2_partitionKey;
  
    public GupsyHospitalInsertInput(Map<String, Object> args) {
      if (args != null) {
        this._CAOS1_partitionKey = (String) args.get("CAOS1_partitionKey");
        this._CAOS2_partitionKey = new GupsyHospitalCaos2_PartitionKeyRelationInput((Map<String, Object>) args.get("CAOS2_partitionKey"));
      }
    }
  
    public String getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public GupsyHospitalCaos2_PartitionKeyRelationInput getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
  }
  public static class FoodDrinkMeasurementQueryInput {
    private Iterable<FoodDrinkMeasurementQueryInput> _OR;
    private Boolean _CAOS2_partitionKey_exists;
    private Object _CAOS2_partitionKey_gt;
    private FoodAccessHouseholdQueryInput _CAOS1_partitionKey;
    private Boolean _CAOS1_partitionKey_exists;
    private Object _CAOS2_partitionKey;
    private Object _CAOS2_partitionKey_lte;
    private Iterable<Object> _CAOS2_partitionKey_in;
    private Iterable<Object> _CAOS2_partitionKey_nin;
    private Object _CAOS2_partitionKey_ne;
    private Object _CAOS2_partitionKey_gte;
    private Object _CAOS2_partitionKey_lt;
    private Iterable<FoodDrinkMeasurementQueryInput> _AND;
  
    public FoodDrinkMeasurementQueryInput(Map<String, Object> args) {
      if (args != null) {
        if (args.get("OR") != null) {
          this._OR = ((List<Map<String, Object>>) args.get("OR")).stream().map(FoodDrinkMeasurementQueryInput::new).collect(Collectors.toList());
        }
        this._CAOS2_partitionKey_exists = (Boolean) args.get("CAOS2_partitionKey_exists");
        this._CAOS2_partitionKey_gt = (Object) args.get("CAOS2_partitionKey_gt");
        this._CAOS1_partitionKey = new FoodAccessHouseholdQueryInput((Map<String, Object>) args.get("CAOS1_partitionKey"));
        this._CAOS1_partitionKey_exists = (Boolean) args.get("CAOS1_partitionKey_exists");
        this._CAOS2_partitionKey = (Object) args.get("CAOS2_partitionKey");
        this._CAOS2_partitionKey_lte = (Object) args.get("CAOS2_partitionKey_lte");
        this._CAOS2_partitionKey_in = (Iterable<Object>) args.get("CAOS2_partitionKey_in");
        this._CAOS2_partitionKey_nin = (Iterable<Object>) args.get("CAOS2_partitionKey_nin");
        this._CAOS2_partitionKey_ne = (Object) args.get("CAOS2_partitionKey_ne");
        this._CAOS2_partitionKey_gte = (Object) args.get("CAOS2_partitionKey_gte");
        this._CAOS2_partitionKey_lt = (Object) args.get("CAOS2_partitionKey_lt");
        if (args.get("AND") != null) {
          this._AND = ((List<Map<String, Object>>) args.get("AND")).stream().map(FoodDrinkMeasurementQueryInput::new).collect(Collectors.toList());
        }
      }
    }
  
    public Iterable<FoodDrinkMeasurementQueryInput> getOr() { return this._OR; }
    public Boolean getCaos2_PartitionKey_Exists() { return this._CAOS2_partitionKey_exists; }
    public Object getCaos2_PartitionKey_Gt() { return this._CAOS2_partitionKey_gt; }
    public FoodAccessHouseholdQueryInput getCaos1_PartitionKey() { return this._CAOS1_partitionKey; }
    public Boolean getCaos1_PartitionKey_Exists() { return this._CAOS1_partitionKey_exists; }
    public Object getCaos2_PartitionKey() { return this._CAOS2_partitionKey; }
    public Object getCaos2_PartitionKey_Lte() { return this._CAOS2_partitionKey_lte; }
    public Iterable<Object> getCaos2_PartitionKey_In() { return this._CAOS2_partitionKey_in; }
    public Iterable<Object> getCaos2_PartitionKey_Nin() { return this._CAOS2_partitionKey_nin; }
    public Object getCaos2_PartitionKey_Ne() { return this._CAOS2_partitionKey_ne; }
    public Object getCaos2_PartitionKey_Gte() { return this._CAOS2_partitionKey_gte; }
    public Object getCaos2_PartitionKey_Lt() { return this._CAOS2_partitionKey_lt; }
    public Iterable<FoodDrinkMeasurementQueryInput> getAnd() { return this._AND; }
  }
  
}
