/* 
* DB Script Tool
* 2021-03-09 01:22:01
* 
* Android DatabaseHelper Java Class for CAOS_Medical_Lab Database
*/ 
package com.package.CAOS_Medical_Lab.helper;

import com.package.CAOS_Medical_Lab.model.CAOS;
import com.package.CAOS_Medical_Lab.model.IMHOTEP_Bizarros;
import com.package.CAOS_Medical_Lab.model.GENE_CRISpy;
import com.package.CAOS_Medical_Lab.model.ED_T_DOIDE;
import com.package.CAOS_Medical_Lab.model.Hysteria_P_TRANCE;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
//import java.util.Date;
//import java.util.Locale;
//import java.sql.*;
//import java.text.SimpleDateFormat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper {

    Context context;

    //Log file
    public static final String LOG = "CAOS_Medical_Lab.log";

    //Database 
    private static final String DATABASE_NAME = "CAOS_Medical_Lab.db";
    private static final int DATABASE_VERSION = 1;

    //------------------------------------
    // TABLES
    //------------------------------------

    // CAOS - Table name 
    public static final String TABLE_CAOS = "CAOS";

    // CAOS - Column names
    private static final String FIELD_CAOS__id = "_id";
    private static final String FIELD_CAOS_chemspider = "chemspider";
    private static final String FIELD_CAOS_volume = "volume";
    private static final String FIELD_CAOS_water = "water";
    private static final String FIELD_CAOS_urineph = "urineph";
    private static final String FIELD_CAOS_animalmodels = "animalmodels";
    private static final String FIELD_CAOS_chimpanzeeanimalmodels = "chimpanzeeanimalmodels";
    private static final String FIELD_CAOS_batanimalmodels = "batanimalmodels";
    private static final String FIELD_CAOS_temperaturedegreescelsius = "temperaturedegreescelsius";
    private static final String FIELD_CAOS_sodium = "sodium";
    private static final String FIELD_CAOS_glucose = "glucose";

    // CAOS - Table create statement
    private static final String CREATE_TABLE_CAOS = "CREATE TABLE " + TABLE_CAOS + 
    "(" + 
        FIELD_CAOS__id + " INTEGER(10) IDENTITY(1, 1) NOT NULL, " + 
        FIELD_CAOS_chemspider + " INTEGER, " + 
        FIELD_CAOS_volume + " REAL(10,0), " + 
        FIELD_CAOS_water + " REAL(10,0), " + 
        FIELD_CAOS_urineph + " REAL(10,0) DEFAULT 7.077, " + 
        FIELD_CAOS_animalmodels + " INTEGER(10), " + 
        FIELD_CAOS_chimpanzeeanimalmodels + " INTEGER(10), " + 
        FIELD_CAOS_batanimalmodels + " INTEGER(10), " + 
        FIELD_CAOS_temperaturedegreescelsius + " REAL(10,0) DEFAULT 25, " + 
        FIELD_CAOS_sodium + " REAL(10,0) DEFAULT 23.001, " + 
        FIELD_CAOS_glucose + " REAL(10,0) DEFAULT 180.156 " + 
    ")";



    // IMHOTEP_Bizarros - Table name 
    public static final String TABLE_IMHOTEP_Bizarros = "IMHOTEP_Bizarros";

    // IMHOTEP_Bizarros - Column names
    private static final String FIELD_IMHOTEP_Bizarros__id = "_id";
    private static final String FIELD_IMHOTEP_Bizarros_hospital = "hospital";
    private static final String FIELD_IMHOTEP_Bizarros_clinic = "clinic";
    private static final String FIELD_IMHOTEP_Bizarros_patient = "patient";
    private static final String FIELD_IMHOTEP_Bizarros_diagnosis = "diagnosis";
    private static final String FIELD_IMHOTEP_Bizarros_diabetes = "diabetes";
    private static final String FIELD_IMHOTEP_Bizarros_prediabetes = "prediabetes";
    private static final String FIELD_IMHOTEP_Bizarros_acutenephritis = "acutenephritis";
    private static final String FIELD_IMHOTEP_Bizarros_type1diabetes = "type1diabetes";
    private static final String FIELD_IMHOTEP_Bizarros_type2diabetes = "type2diabetes";
    private static final String FIELD_IMHOTEP_Bizarros_hypertension = "hypertension";

    // IMHOTEP_Bizarros - Table create statement
    private static final String CREATE_TABLE_IMHOTEP_Bizarros = "CREATE TABLE " + TABLE_IMHOTEP_Bizarros + 
    "(" + 
        FIELD_IMHOTEP_Bizarros__id + " INTEGER(10) IDENTITY(1, 1) DEFAULT 32 NOT NULL, " + 
        FIELD_IMHOTEP_Bizarros_hospital + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_clinic + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_patient + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_diagnosis + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_diabetes + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_prediabetes + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_acutenephritis + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_type1diabetes + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_type2diabetes + " INTEGER(10), " + 
        FIELD_IMHOTEP_Bizarros_hypertension + " REAL(10,0) " + 
    ")";



    // GENE_CRISpy - Table name 
    public static final String TABLE_GENE_CRISpy = "GENE_CRISpy";

    // GENE_CRISpy - Column names
    private static final String FIELD_GENE_CRISpy__id = "_id";
    private static final String FIELD_GENE_CRISpy_gene = "gene";
    private static final String FIELD_GENE_CRISpy_virus = "virus";
    private static final String FIELD_GENE_CRISpy_cancer = "cancer";
    private static final String FIELD_GENE_CRISpy_protein = "protein";
    private static final String FIELD_GENE_CRISpy_humancellmodel = "humancellmodel";
    private static final String FIELD_GENE_CRISpy_coronavirus = "coronavirus";
    private static final String FIELD_GENE_CRISpy_sarscov2 = "sarscov2";
    private static final String FIELD_GENE_CRISpy_kidneystones = "kidneystones";
    private static final String FIELD_GENE_CRISpy_pneumonia = "pneumonia";
    private static final String FIELD_GENE_CRISpy_humanenzymes = "humanenzymes";
    private static final String FIELD_GENE_CRISpy_ghrelin = "ghrelin";
    private static final String FIELD_GENE_CRISpy_obestatin = "obestatin";
    private static final String FIELD_GENE_CRISpy_breastcancergene = "breastcancergene";
    private static final String FIELD_GENE_CRISpy_disease = "disease";
    private static final String FIELD_GENE_CRISpy_cardiovasculardisease = "cardiovasculardisease";
    private static final String FIELD_GENE_CRISpy_grampositivestain = "grampositivestain";
    private static final String FIELD_GENE_CRISpy_gramnegativestain = "gramnegativestain";
    private static final String FIELD_GENE_CRISpy_denaturation = "denaturation";
    private static final String FIELD_GENE_CRISpy_dna = "dna";
    private static final String FIELD_GENE_CRISpy_rna = "rna";
    private static final String FIELD_GENE_CRISpy_lcarnitine = "lcarnitine";

    // GENE_CRISpy - Table create statement
    private static final String CREATE_TABLE_GENE_CRISpy = "CREATE TABLE " + TABLE_GENE_CRISpy + 
    "(" + 
        FIELD_GENE_CRISpy__id + " INTEGER(10) IDENTITY(1, 1) DEFAULT 32 NOT NULL, " + 
        FIELD_GENE_CRISpy_gene + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_virus + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_cancer + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_protein + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_humancellmodel + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_coronavirus + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_sarscov2 + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_kidneystones + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_pneumonia + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_humanenzymes + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_ghrelin + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_obestatin + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_breastcancergene + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_disease + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_cardiovasculardisease + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_grampositivestain + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_gramnegativestain + " INTEGER(10), " + 
        FIELD_GENE_CRISpy_denaturation + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_dna + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_rna + " REAL(10,0), " + 
        FIELD_GENE_CRISpy_lcarnitine + " REAL(10,0) " + 
    ")";



    // ED_T_DOIDE - Table name 
    public static final String TABLE_ED_T_DOIDE = "ED_T_DOIDE";

    // ED_T_DOIDE - Column names
    private static final String FIELD_ED_T_DOIDE__id = "_id";
    private static final String FIELD_ED_T_DOIDE_nutritionix = "nutritionix";
    private static final String FIELD_ED_T_DOIDE_mcdata = "mcdata";
    private static final String FIELD_ED_T_DOIDE_kentuckyfriedchicken = "kentuckyfriedchicken";
    private static final String FIELD_ED_T_DOIDE_dominospizza = "dominospizza";

    // ED_T_DOIDE - Table create statement
    private static final String CREATE_TABLE_ED_T_DOIDE = "CREATE TABLE " + TABLE_ED_T_DOIDE + 
    "(" + 
        FIELD_ED_T_DOIDE__id + " INTEGER PRIMARY KEY, " + 
        FIELD_ED_T_DOIDE_nutritionix + " REAL(10,0), " + 
        FIELD_ED_T_DOIDE_mcdata + " REAL(10,0), " + 
        FIELD_ED_T_DOIDE_kentuckyfriedchicken + " REAL(10,0), " + 
        FIELD_ED_T_DOIDE_dominospizza + " REAL(10,0) " + 
    ")";



    // Hysteria_P_TRANCE - Table name 
    public static final String TABLE_Hysteria_P_TRANCE = "Hysteria_P_TRANCE";

    // Hysteria_P_TRANCE - Column names
    private static final String FIELD_Hysteria_P_TRANCE__id = "_id";
    private static final String FIELD_Hysteria_P_TRANCE_fitbit = "fitbit";
    private static final String FIELD_Hysteria_P_TRANCE_sleep = "sleep";
    private static final String FIELD_Hysteria_P_TRANCE_weight = "weight";
    private static final String FIELD_Hysteria_P_TRANCE_heartrate = "heartrate";
    private static final String FIELD_Hysteria_P_TRANCE_dailyactivity = "dailyactivity";
    private static final String FIELD_Hysteria_P_TRANCE_waterintake = "waterintake";

    // Hysteria_P_TRANCE - Table create statement
    private static final String CREATE_TABLE_Hysteria_P_TRANCE = "CREATE TABLE " + TABLE_Hysteria_P_TRANCE + 
    "(" + 
        FIELD_Hysteria_P_TRANCE__id + " INTEGER PRIMARY KEY, " + 
        FIELD_Hysteria_P_TRANCE_fitbit + " INTEGER(10) DEFAULT 32, " + 
        FIELD_Hysteria_P_TRANCE_sleep + " REAL(10,0), " + 
        FIELD_Hysteria_P_TRANCE_weight + " REAL(10,0), " + 
        FIELD_Hysteria_P_TRANCE_heartrate + " INTEGER(10) DEFAULT 32, " + 
        FIELD_Hysteria_P_TRANCE_dailyactivity + " REAL(10,0), " + 
        FIELD_Hysteria_P_TRANCE_waterintake + " REAL(10,0) DEFAULT 18.001 " + 
    ")";



    //------------------------------------
    // CONSTRUCTORS
    //------------------------------------

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_CAOS);
        db.execSQL(CREATE_TABLE_IMHOTEP_Bizarros);
        db.execSQL(CREATE_TABLE_GENE_CRISpy);
        db.execSQL(CREATE_TABLE_ED_T_DOIDE);
        db.execSQL(CREATE_TABLE_Hysteria_P_TRANCE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {

            case 1: // from v1 -> v2

            case 2: // from v2 -> v3

        }
    }

    // Closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();

        if (db != null && db.isOpen()) db.close();
    }

    //------------------------------------
    // CAOS - OPERATIONS
    //------------------------------------

    /*
     *  create or update a CAOS row using an object (Model Class)
     */
    public long saveCAOS(CAOS mCAOS) { 

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(FIELD_CAOS__id, mCAOS.get_id());
        values.put(FIELD_CAOS_chemspider, mCAOS.getChemspider());
        values.put(FIELD_CAOS_volume, mCAOS.getVolume());
        values.put(FIELD_CAOS_water, mCAOS.getWater());
        values.put(FIELD_CAOS_urineph, mCAOS.getUrineph());
        values.put(FIELD_CAOS_animalmodels, mCAOS.getAnimalmodels());
        values.put(FIELD_CAOS_chimpanzeeanimalmodels, mCAOS.getChimpanzeeanimalmodels());
        values.put(FIELD_CAOS_batanimalmodels, mCAOS.getBatanimalmodels());
        values.put(FIELD_CAOS_temperaturedegreescelsius, mCAOS.getTemperaturedegreescelsius());
        values.put(FIELD_CAOS_sodium, mCAOS.getSodium());
        values.put(FIELD_CAOS_glucose, mCAOS.getGlucose());

        long id = 0; 

        if (mCAOS.get_id() > 0) {    

            // updating row
            db.update(TABLE_CAOS, values, FIELD_CAOS__id + "=?", new String[] {String.valueOf(mCAOS.get_id())});

            id = mCAOS.get_id();

        } else {

            // inserting row
            id = db.insert(TABLE_CAOS, null, values);

        }

        return id;
    }



    /*
     *  delete a CAOS record by ID
     */
    public void deleteCAOS(long id) { 

        if (id > 0) {    

            SQLiteDatabase db = this.getWritableDatabase();

            // deleting row
            db.delete(TABLE_CAOS, FIELD_CAOS__id + "=?", new String[] {String.valueOf(id)});

        }

    }



    /*
     *  getting a single CAOS object by ID
     */
    public CAOS getCAOS(long id) { 

        CAOS mCAOS = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_CAOS + " WHERE " + FIELD_CAOS__id + "=" + id;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {

                mCAOS = new CAOS();
                mCAOS.set_id(cur.getLong(cur.getColumnIndex(FIELD_CAOS__id)));
                mCAOS.setChemspider(cur.getInt(cur.getColumnIndex(FIELD_CAOS_chemspider)));
                mCAOS.setVolume(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_volume)));
                mCAOS.setWater(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_water)));
                mCAOS.setUrineph(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_urineph)));
                mCAOS.setAnimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_animalmodels)));
                mCAOS.setChimpanzeeanimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_chimpanzeeanimalmodels)));
                mCAOS.setBatanimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_batanimalmodels)));
                mCAOS.setTemperaturedegreescelsius(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_temperaturedegreescelsius)));
                mCAOS.setSodium(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_sodium)));
                mCAOS.setGlucose(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_glucose)));

            }
            cur.close();
        }

        return mCAOS;
    }



    /*
     *  getting all CAOS objects
     */
    public List<CAOS> getAllCAOS() { 

        List<CAOS> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_CAOS;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    CAOS mCAOS = new CAOS();
                    mCAOS.set_id(cur.getLong(cur.getColumnIndex(FIELD_CAOS__id)));
                    mCAOS.setChemspider(cur.getInt(cur.getColumnIndex(FIELD_CAOS_chemspider)));
                    mCAOS.setVolume(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_volume)));
                    mCAOS.setWater(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_water)));
                    mCAOS.setUrineph(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_urineph)));
                    mCAOS.setAnimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_animalmodels)));
                    mCAOS.setChimpanzeeanimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_chimpanzeeanimalmodels)));
                    mCAOS.setBatanimalmodels(cur.getLong(cur.getColumnIndex(FIELD_CAOS_batanimalmodels)));
                    mCAOS.setTemperaturedegreescelsius(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_temperaturedegreescelsius)));
                    mCAOS.setSodium(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_sodium)));
                    mCAOS.setGlucose(cur.getDouble(cur.getColumnIndex(FIELD_CAOS_glucose)));

                    list.add(mCAOS); // adding objects to the list

                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }



    /*
     *  getting CAOS rows as mapped list for ListViews
     */
    public List<Map<String, String>> getListCAOS(List<CAOS> list) { 

        List<Map<String, String>> mArrayList = new ArrayList<>();

        for (CAOS mCAOS : list) {

            HashMap<String, String> map = new HashMap<>();

            map.put("CAOS__id", String.valueOf(mCAOS.get_id()));
            map.put("CAOS_chemspider", String.valueOf(mCAOS.getChemspider()));
            map.put("CAOS_volume", String.valueOf(mCAOS.getVolume()));
            map.put("CAOS_water", String.valueOf(mCAOS.getWater()));
            map.put("CAOS_urineph", String.valueOf(mCAOS.getUrineph()));
            map.put("CAOS_animalmodels", String.valueOf(mCAOS.getAnimalmodels()));
            map.put("CAOS_chimpanzeeanimalmodels", String.valueOf(mCAOS.getChimpanzeeanimalmodels()));
            map.put("CAOS_batanimalmodels", String.valueOf(mCAOS.getBatanimalmodels()));
            map.put("CAOS_temperaturedegreescelsius", String.valueOf(mCAOS.getTemperaturedegreescelsius()));
            map.put("CAOS_sodium", String.valueOf(mCAOS.getSodium()));
            map.put("CAOS_glucose", String.valueOf(mCAOS.getGlucose()));

            mArrayList.add(map);
        }

        return mArrayList;
    }



    //------------------------------------
    // IMHOTEP_BIZARROS - OPERATIONS
    //------------------------------------

    /*
     *  create or update a IMHOTEP_Bizarros row using an object (Model Class)
     */
    public long saveIMHOTEP_Bizarros(IMHOTEP_Bizarros mIMHOTEP_Bizarros) { 

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(FIELD_IMHOTEP_Bizarros__id, mIMHOTEP_Bizarros.get_id());
        values.put(FIELD_IMHOTEP_Bizarros_hospital, mIMHOTEP_Bizarros.getHospital());
        values.put(FIELD_IMHOTEP_Bizarros_clinic, mIMHOTEP_Bizarros.getClinic());
        values.put(FIELD_IMHOTEP_Bizarros_patient, mIMHOTEP_Bizarros.getPatient());
        values.put(FIELD_IMHOTEP_Bizarros_diagnosis, mIMHOTEP_Bizarros.getDiagnosis());
        values.put(FIELD_IMHOTEP_Bizarros_diabetes, mIMHOTEP_Bizarros.getDiabetes());
        values.put(FIELD_IMHOTEP_Bizarros_prediabetes, mIMHOTEP_Bizarros.getPrediabetes());
        values.put(FIELD_IMHOTEP_Bizarros_acutenephritis, mIMHOTEP_Bizarros.getAcutenephritis());
        values.put(FIELD_IMHOTEP_Bizarros_type1diabetes, mIMHOTEP_Bizarros.getType1diabetes());
        values.put(FIELD_IMHOTEP_Bizarros_type2diabetes, mIMHOTEP_Bizarros.getType2diabetes());
        values.put(FIELD_IMHOTEP_Bizarros_hypertension, mIMHOTEP_Bizarros.getHypertension());

        long id = 0; 

        if (mIMHOTEP_Bizarros.get_id() > 0) {    

            // updating row
            db.update(TABLE_IMHOTEP_Bizarros, values, FIELD_IMHOTEP_Bizarros__id + "=?", new String[] {String.valueOf(mIMHOTEP_Bizarros.get_id())});

            id = mIMHOTEP_Bizarros.get_id();

        } else {

            // inserting row
            id = db.insert(TABLE_IMHOTEP_Bizarros, null, values);

        }

        return id;
    }



    /*
     *  delete a IMHOTEP_Bizarros record by ID
     */
    public void deleteIMHOTEP_Bizarros(long id) { 

        if (id > 0) {    

            SQLiteDatabase db = this.getWritableDatabase();

            // deleting row
            db.delete(TABLE_IMHOTEP_Bizarros, FIELD_IMHOTEP_Bizarros__id + "=?", new String[] {String.valueOf(id)});

        }

    }



    /*
     *  getting a single IMHOTEP_Bizarros object by ID
     */
    public IMHOTEP_Bizarros getIMHOTEP_Bizarros(long id) { 

        IMHOTEP_Bizarros mIMHOTEP_Bizarros = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_IMHOTEP_Bizarros + " WHERE " + FIELD_IMHOTEP_Bizarros__id + "=" + id;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {

                mIMHOTEP_Bizarros = new IMHOTEP_Bizarros();
                mIMHOTEP_Bizarros.set_id(cur.getInt(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros__id)));
                mIMHOTEP_Bizarros.setHospital(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_hospital)));
                mIMHOTEP_Bizarros.setClinic(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_clinic)));
                mIMHOTEP_Bizarros.setPatient(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_patient)));
                mIMHOTEP_Bizarros.setDiagnosis(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_diagnosis)));
                mIMHOTEP_Bizarros.setDiabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_diabetes)));
                mIMHOTEP_Bizarros.setPrediabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_prediabetes)));
                mIMHOTEP_Bizarros.setAcutenephritis(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_acutenephritis)));
                mIMHOTEP_Bizarros.setType1diabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_type1diabetes)));
                mIMHOTEP_Bizarros.setType2diabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_type2diabetes)));
                mIMHOTEP_Bizarros.setHypertension(cur.getDouble(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_hypertension)));

            }
            cur.close();
        }

        return mIMHOTEP_Bizarros;
    }



    /*
     *  getting all IMHOTEP_Bizarros objects
     */
    public List<IMHOTEP_Bizarros> getAllIMHOTEP_Bizarros() { 

        List<IMHOTEP_Bizarros> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_IMHOTEP_Bizarros;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    IMHOTEP_Bizarros mIMHOTEP_Bizarros = new IMHOTEP_Bizarros();
                    mIMHOTEP_Bizarros.set_id(cur.getInt(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros__id)));
                    mIMHOTEP_Bizarros.setHospital(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_hospital)));
                    mIMHOTEP_Bizarros.setClinic(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_clinic)));
                    mIMHOTEP_Bizarros.setPatient(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_patient)));
                    mIMHOTEP_Bizarros.setDiagnosis(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_diagnosis)));
                    mIMHOTEP_Bizarros.setDiabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_diabetes)));
                    mIMHOTEP_Bizarros.setPrediabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_prediabetes)));
                    mIMHOTEP_Bizarros.setAcutenephritis(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_acutenephritis)));
                    mIMHOTEP_Bizarros.setType1diabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_type1diabetes)));
                    mIMHOTEP_Bizarros.setType2diabetes(cur.getLong(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_type2diabetes)));
                    mIMHOTEP_Bizarros.setHypertension(cur.getDouble(cur.getColumnIndex(FIELD_IMHOTEP_Bizarros_hypertension)));

                    list.add(mIMHOTEP_Bizarros); // adding objects to the list

                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }



    /*
     *  getting IMHOTEP_Bizarros rows as mapped list for ListViews
     */
    public List<Map<String, String>> getListIMHOTEP_Bizarros(List<IMHOTEP_Bizarros> list) { 

        List<Map<String, String>> mArrayList = new ArrayList<>();

        for (IMHOTEP_Bizarros mIMHOTEP_Bizarros : list) {

            HashMap<String, String> map = new HashMap<>();

            map.put("IMHOTEP_Bizarros__id", String.valueOf(mIMHOTEP_Bizarros.get_id()));
            map.put("IMHOTEP_Bizarros_hospital", String.valueOf(mIMHOTEP_Bizarros.getHospital()));
            map.put("IMHOTEP_Bizarros_clinic", String.valueOf(mIMHOTEP_Bizarros.getClinic()));
            map.put("IMHOTEP_Bizarros_patient", String.valueOf(mIMHOTEP_Bizarros.getPatient()));
            map.put("IMHOTEP_Bizarros_diagnosis", String.valueOf(mIMHOTEP_Bizarros.getDiagnosis()));
            map.put("IMHOTEP_Bizarros_diabetes", String.valueOf(mIMHOTEP_Bizarros.getDiabetes()));
            map.put("IMHOTEP_Bizarros_prediabetes", String.valueOf(mIMHOTEP_Bizarros.getPrediabetes()));
            map.put("IMHOTEP_Bizarros_acutenephritis", String.valueOf(mIMHOTEP_Bizarros.getAcutenephritis()));
            map.put("IMHOTEP_Bizarros_type1diabetes", String.valueOf(mIMHOTEP_Bizarros.getType1diabetes()));
            map.put("IMHOTEP_Bizarros_type2diabetes", String.valueOf(mIMHOTEP_Bizarros.getType2diabetes()));
            map.put("IMHOTEP_Bizarros_hypertension", String.valueOf(mIMHOTEP_Bizarros.getHypertension()));

            mArrayList.add(map);
        }

        return mArrayList;
    }



    //------------------------------------
    // GENE_CRISPY - OPERATIONS
    //------------------------------------

    /*
     *  create or update a GENE_CRISpy row using an object (Model Class)
     */
    public long saveGENE_CRISpy(GENE_CRISpy mGENE_CRISpy) { 

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(FIELD_GENE_CRISpy__id, mGENE_CRISpy.get_id());
        values.put(FIELD_GENE_CRISpy_gene, mGENE_CRISpy.getGene());
        values.put(FIELD_GENE_CRISpy_virus, mGENE_CRISpy.getVirus());
        values.put(FIELD_GENE_CRISpy_cancer, mGENE_CRISpy.getCancer());
        values.put(FIELD_GENE_CRISpy_protein, mGENE_CRISpy.getProtein());
        values.put(FIELD_GENE_CRISpy_humancellmodel, mGENE_CRISpy.getHumancellmodel());
        values.put(FIELD_GENE_CRISpy_coronavirus, mGENE_CRISpy.getCoronavirus());
        values.put(FIELD_GENE_CRISpy_sarscov2, mGENE_CRISpy.getSarscov2());
        values.put(FIELD_GENE_CRISpy_kidneystones, mGENE_CRISpy.getKidneystones());
        values.put(FIELD_GENE_CRISpy_pneumonia, mGENE_CRISpy.getPneumonia());
        values.put(FIELD_GENE_CRISpy_humanenzymes, mGENE_CRISpy.getHumanenzymes());
        values.put(FIELD_GENE_CRISpy_ghrelin, mGENE_CRISpy.getGhrelin());
        values.put(FIELD_GENE_CRISpy_obestatin, mGENE_CRISpy.getObestatin());
        values.put(FIELD_GENE_CRISpy_breastcancergene, mGENE_CRISpy.getBreastcancergene());
        values.put(FIELD_GENE_CRISpy_disease, mGENE_CRISpy.getDisease());
        values.put(FIELD_GENE_CRISpy_cardiovasculardisease, mGENE_CRISpy.getCardiovasculardisease());
        values.put(FIELD_GENE_CRISpy_grampositivestain, mGENE_CRISpy.getGrampositivestain());
        values.put(FIELD_GENE_CRISpy_gramnegativestain, mGENE_CRISpy.getGramnegativestain());
        values.put(FIELD_GENE_CRISpy_denaturation, mGENE_CRISpy.getDenaturation());
        values.put(FIELD_GENE_CRISpy_dna, mGENE_CRISpy.getDna());
        values.put(FIELD_GENE_CRISpy_rna, mGENE_CRISpy.getRna());
        values.put(FIELD_GENE_CRISpy_lcarnitine, mGENE_CRISpy.getLcarnitine());

        long id = 0; 

        if (mGENE_CRISpy.get_id() > 0) {    

            // updating row
            db.update(TABLE_GENE_CRISpy, values, FIELD_GENE_CRISpy__id + "=?", new String[] {String.valueOf(mGENE_CRISpy.get_id())});

            id = mGENE_CRISpy.get_id();

        } else {

            // inserting row
            id = db.insert(TABLE_GENE_CRISpy, null, values);

        }

        return id;
    }



    /*
     *  delete a GENE_CRISpy record by ID
     */
    public void deleteGENE_CRISpy(long id) { 

        if (id > 0) {    

            SQLiteDatabase db = this.getWritableDatabase();

            // deleting row
            db.delete(TABLE_GENE_CRISpy, FIELD_GENE_CRISpy__id + "=?", new String[] {String.valueOf(id)});

        }

    }



    /*
     *  getting a single GENE_CRISpy object by ID
     */
    public GENE_CRISpy getGENE_CRISpy(long id) { 

        GENE_CRISpy mGENE_CRISpy = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_GENE_CRISpy + " WHERE " + FIELD_GENE_CRISpy__id + "=" + id;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {

                mGENE_CRISpy = new GENE_CRISpy();
                mGENE_CRISpy.set_id(cur.getInt(cur.getColumnIndex(FIELD_GENE_CRISpy__id)));
                mGENE_CRISpy.setGene(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_gene)));
                mGENE_CRISpy.setVirus(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_virus)));
                mGENE_CRISpy.setCancer(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_cancer)));
                mGENE_CRISpy.setProtein(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_protein)));
                mGENE_CRISpy.setHumancellmodel(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_humancellmodel)));
                mGENE_CRISpy.setCoronavirus(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_coronavirus)));
                mGENE_CRISpy.setSarscov2(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_sarscov2)));
                mGENE_CRISpy.setKidneystones(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_kidneystones)));
                mGENE_CRISpy.setPneumonia(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_pneumonia)));
                mGENE_CRISpy.setHumanenzymes(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_humanenzymes)));
                mGENE_CRISpy.setGhrelin(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_ghrelin)));
                mGENE_CRISpy.setObestatin(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_obestatin)));
                mGENE_CRISpy.setBreastcancergene(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_breastcancergene)));
                mGENE_CRISpy.setDisease(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_disease)));
                mGENE_CRISpy.setCardiovasculardisease(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_cardiovasculardisease)));
                mGENE_CRISpy.setGrampositivestain(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_grampositivestain)));
                mGENE_CRISpy.setGramnegativestain(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_gramnegativestain)));
                mGENE_CRISpy.setDenaturation(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_denaturation)));
                mGENE_CRISpy.setDna(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_dna)));
                mGENE_CRISpy.setRna(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_rna)));
                mGENE_CRISpy.setLcarnitine(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_lcarnitine)));

            }
            cur.close();
        }

        return mGENE_CRISpy;
    }



    /*
     *  getting all GENE_CRISpy objects
     */
    public List<GENE_CRISpy> getAllGENE_CRISpy() { 

        List<GENE_CRISpy> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_GENE_CRISpy;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    GENE_CRISpy mGENE_CRISpy = new GENE_CRISpy();
                    mGENE_CRISpy.set_id(cur.getInt(cur.getColumnIndex(FIELD_GENE_CRISpy__id)));
                    mGENE_CRISpy.setGene(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_gene)));
                    mGENE_CRISpy.setVirus(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_virus)));
                    mGENE_CRISpy.setCancer(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_cancer)));
                    mGENE_CRISpy.setProtein(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_protein)));
                    mGENE_CRISpy.setHumancellmodel(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_humancellmodel)));
                    mGENE_CRISpy.setCoronavirus(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_coronavirus)));
                    mGENE_CRISpy.setSarscov2(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_sarscov2)));
                    mGENE_CRISpy.setKidneystones(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_kidneystones)));
                    mGENE_CRISpy.setPneumonia(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_pneumonia)));
                    mGENE_CRISpy.setHumanenzymes(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_humanenzymes)));
                    mGENE_CRISpy.setGhrelin(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_ghrelin)));
                    mGENE_CRISpy.setObestatin(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_obestatin)));
                    mGENE_CRISpy.setBreastcancergene(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_breastcancergene)));
                    mGENE_CRISpy.setDisease(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_disease)));
                    mGENE_CRISpy.setCardiovasculardisease(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_cardiovasculardisease)));
                    mGENE_CRISpy.setGrampositivestain(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_grampositivestain)));
                    mGENE_CRISpy.setGramnegativestain(cur.getLong(cur.getColumnIndex(FIELD_GENE_CRISpy_gramnegativestain)));
                    mGENE_CRISpy.setDenaturation(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_denaturation)));
                    mGENE_CRISpy.setDna(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_dna)));
                    mGENE_CRISpy.setRna(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_rna)));
                    mGENE_CRISpy.setLcarnitine(cur.getDouble(cur.getColumnIndex(FIELD_GENE_CRISpy_lcarnitine)));

                    list.add(mGENE_CRISpy); // adding objects to the list

                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }



    /*
     *  getting GENE_CRISpy rows as mapped list for ListViews
     */
    public List<Map<String, String>> getListGENE_CRISpy(List<GENE_CRISpy> list) { 

        List<Map<String, String>> mArrayList = new ArrayList<>();

        for (GENE_CRISpy mGENE_CRISpy : list) {

            HashMap<String, String> map = new HashMap<>();

            map.put("GENE_CRISpy__id", String.valueOf(mGENE_CRISpy.get_id()));
            map.put("GENE_CRISpy_gene", String.valueOf(mGENE_CRISpy.getGene()));
            map.put("GENE_CRISpy_virus", String.valueOf(mGENE_CRISpy.getVirus()));
            map.put("GENE_CRISpy_cancer", String.valueOf(mGENE_CRISpy.getCancer()));
            map.put("GENE_CRISpy_protein", String.valueOf(mGENE_CRISpy.getProtein()));
            map.put("GENE_CRISpy_humancellmodel", String.valueOf(mGENE_CRISpy.getHumancellmodel()));
            map.put("GENE_CRISpy_coronavirus", String.valueOf(mGENE_CRISpy.getCoronavirus()));
            map.put("GENE_CRISpy_sarscov2", String.valueOf(mGENE_CRISpy.getSarscov2()));
            map.put("GENE_CRISpy_kidneystones", String.valueOf(mGENE_CRISpy.getKidneystones()));
            map.put("GENE_CRISpy_pneumonia", String.valueOf(mGENE_CRISpy.getPneumonia()));
            map.put("GENE_CRISpy_humanenzymes", String.valueOf(mGENE_CRISpy.getHumanenzymes()));
            map.put("GENE_CRISpy_ghrelin", String.valueOf(mGENE_CRISpy.getGhrelin()));
            map.put("GENE_CRISpy_obestatin", String.valueOf(mGENE_CRISpy.getObestatin()));
            map.put("GENE_CRISpy_breastcancergene", String.valueOf(mGENE_CRISpy.getBreastcancergene()));
            map.put("GENE_CRISpy_disease", String.valueOf(mGENE_CRISpy.getDisease()));
            map.put("GENE_CRISpy_cardiovasculardisease", String.valueOf(mGENE_CRISpy.getCardiovasculardisease()));
            map.put("GENE_CRISpy_grampositivestain", String.valueOf(mGENE_CRISpy.getGrampositivestain()));
            map.put("GENE_CRISpy_gramnegativestain", String.valueOf(mGENE_CRISpy.getGramnegativestain()));
            map.put("GENE_CRISpy_denaturation", String.valueOf(mGENE_CRISpy.getDenaturation()));
            map.put("GENE_CRISpy_dna", String.valueOf(mGENE_CRISpy.getDna()));
            map.put("GENE_CRISpy_rna", String.valueOf(mGENE_CRISpy.getRna()));
            map.put("GENE_CRISpy_lcarnitine", String.valueOf(mGENE_CRISpy.getLcarnitine()));

            mArrayList.add(map);
        }

        return mArrayList;
    }



    //------------------------------------
    // ED_T_DOIDE - OPERATIONS
    //------------------------------------

    /*
     *  create or update a ED_T_DOIDE row using an object (Model Class)
     */
    public long saveED_T_DOIDE(ED_T_DOIDE mED_T_DOIDE) { 

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(FIELD_ED_T_DOIDE__id, mED_T_DOIDE.get_id());
        values.put(FIELD_ED_T_DOIDE_nutritionix, mED_T_DOIDE.getNutritionix());
        values.put(FIELD_ED_T_DOIDE_mcdata, mED_T_DOIDE.getMcdata());
        values.put(FIELD_ED_T_DOIDE_kentuckyfriedchicken, mED_T_DOIDE.getKentuckyfriedchicken());
        values.put(FIELD_ED_T_DOIDE_dominospizza, mED_T_DOIDE.getDominospizza());

        long id = 0; 

        if (mED_T_DOIDE.get_id() > 0) {    

            // updating row
            db.update(TABLE_ED_T_DOIDE, values, FIELD_ED_T_DOIDE__id + "=?", new String[] {String.valueOf(mED_T_DOIDE.get_id())});

            id = mED_T_DOIDE.get_id();

        } else {

            // inserting row
            id = db.insert(TABLE_ED_T_DOIDE, null, values);

        }

        return id;
    }



    /*
     *  delete a ED_T_DOIDE record by ID
     */
    public void deleteED_T_DOIDE(long id) { 

        if (id > 0) {    

            SQLiteDatabase db = this.getWritableDatabase();

            // deleting row
            db.delete(TABLE_ED_T_DOIDE, FIELD_ED_T_DOIDE__id + "=?", new String[] {String.valueOf(id)});

        }

    }



    /*
     *  getting a single ED_T_DOIDE object by ID
     */
    public ED_T_DOIDE getED_T_DOIDE(long id) { 

        ED_T_DOIDE mED_T_DOIDE = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_ED_T_DOIDE + " WHERE " + FIELD_ED_T_DOIDE__id + "=" + id;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {

                mED_T_DOIDE = new ED_T_DOIDE();
                mED_T_DOIDE.set_id(cur.getInt(cur.getColumnIndex(FIELD_ED_T_DOIDE__id)));
                mED_T_DOIDE.setNutritionix(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_nutritionix)));
                mED_T_DOIDE.setMcdata(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_mcdata)));
                mED_T_DOIDE.setKentuckyfriedchicken(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_kentuckyfriedchicken)));
                mED_T_DOIDE.setDominospizza(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_dominospizza)));

            }
            cur.close();
        }

        return mED_T_DOIDE;
    }



    /*
     *  getting all ED_T_DOIDE objects
     */
    public List<ED_T_DOIDE> getAllED_T_DOIDE() { 

        List<ED_T_DOIDE> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_ED_T_DOIDE;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    ED_T_DOIDE mED_T_DOIDE = new ED_T_DOIDE();
                    mED_T_DOIDE.set_id(cur.getInt(cur.getColumnIndex(FIELD_ED_T_DOIDE__id)));
                    mED_T_DOIDE.setNutritionix(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_nutritionix)));
                    mED_T_DOIDE.setMcdata(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_mcdata)));
                    mED_T_DOIDE.setKentuckyfriedchicken(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_kentuckyfriedchicken)));
                    mED_T_DOIDE.setDominospizza(cur.getDouble(cur.getColumnIndex(FIELD_ED_T_DOIDE_dominospizza)));

                    list.add(mED_T_DOIDE); // adding objects to the list

                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }



    /*
     *  getting ED_T_DOIDE rows as mapped list for ListViews
     */
    public List<Map<String, String>> getListED_T_DOIDE(List<ED_T_DOIDE> list) { 

        List<Map<String, String>> mArrayList = new ArrayList<>();

        for (ED_T_DOIDE mED_T_DOIDE : list) {

            HashMap<String, String> map = new HashMap<>();

            map.put("ED_T_DOIDE__id", String.valueOf(mED_T_DOIDE.get_id()));
            map.put("ED_T_DOIDE_nutritionix", String.valueOf(mED_T_DOIDE.getNutritionix()));
            map.put("ED_T_DOIDE_mcdata", String.valueOf(mED_T_DOIDE.getMcdata()));
            map.put("ED_T_DOIDE_kentuckyfriedchicken", String.valueOf(mED_T_DOIDE.getKentuckyfriedchicken()));
            map.put("ED_T_DOIDE_dominospizza", String.valueOf(mED_T_DOIDE.getDominospizza()));

            mArrayList.add(map);
        }

        return mArrayList;
    }



    //------------------------------------
    // HYSTERIA_P_TRANCE - OPERATIONS
    //------------------------------------

    /*
     *  create or update a Hysteria_P_TRANCE row using an object (Model Class)
     */
    public long saveHysteria_P_TRANCE(Hysteria_P_TRANCE mHysteria_P_TRANCE) { 

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(FIELD_Hysteria_P_TRANCE__id, mHysteria_P_TRANCE.get_id());
        values.put(FIELD_Hysteria_P_TRANCE_fitbit, mHysteria_P_TRANCE.getFitbit());
        values.put(FIELD_Hysteria_P_TRANCE_sleep, mHysteria_P_TRANCE.getSleep());
        values.put(FIELD_Hysteria_P_TRANCE_weight, mHysteria_P_TRANCE.getWeight());
        values.put(FIELD_Hysteria_P_TRANCE_heartrate, mHysteria_P_TRANCE.getHeartrate());
        values.put(FIELD_Hysteria_P_TRANCE_dailyactivity, mHysteria_P_TRANCE.getDailyactivity());
        values.put(FIELD_Hysteria_P_TRANCE_waterintake, mHysteria_P_TRANCE.getWaterintake());

        long id = 0; 

        if (mHysteria_P_TRANCE.get_id() > 0) {    

            // updating row
            db.update(TABLE_Hysteria_P_TRANCE, values, FIELD_Hysteria_P_TRANCE__id + "=?", new String[] {String.valueOf(mHysteria_P_TRANCE.get_id())});

            id = mHysteria_P_TRANCE.get_id();

        } else {

            // inserting row
            id = db.insert(TABLE_Hysteria_P_TRANCE, null, values);

        }

        return id;
    }



    /*
     *  delete a Hysteria_P_TRANCE record by ID
     */
    public void deleteHysteria_P_TRANCE(long id) { 

        if (id > 0) {    

            SQLiteDatabase db = this.getWritableDatabase();

            // deleting row
            db.delete(TABLE_Hysteria_P_TRANCE, FIELD_Hysteria_P_TRANCE__id + "=?", new String[] {String.valueOf(id)});

        }

    }



    /*
     *  getting a single Hysteria_P_TRANCE object by ID
     */
    public Hysteria_P_TRANCE getHysteria_P_TRANCE(long id) { 

        Hysteria_P_TRANCE mHysteria_P_TRANCE = null;

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_Hysteria_P_TRANCE + " WHERE " + FIELD_Hysteria_P_TRANCE__id + "=" + id;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {

                mHysteria_P_TRANCE = new Hysteria_P_TRANCE();
                mHysteria_P_TRANCE.set_id(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE__id)));
                mHysteria_P_TRANCE.setFitbit(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_fitbit)));
                mHysteria_P_TRANCE.setSleep(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_sleep)));
                mHysteria_P_TRANCE.setWeight(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_weight)));
                mHysteria_P_TRANCE.setHeartrate(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_heartrate)));
                mHysteria_P_TRANCE.setDailyactivity(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_dailyactivity)));
                mHysteria_P_TRANCE.setWaterintake(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_waterintake)));

            }
            cur.close();
        }

        return mHysteria_P_TRANCE;
    }



    /*
     *  getting all Hysteria_P_TRANCE objects
     */
    public List<Hysteria_P_TRANCE> getAllHysteria_P_TRANCE() { 

        List<Hysteria_P_TRANCE> list = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_Hysteria_P_TRANCE;
        //Log.i(LOG, selectQuery);

        Cursor cur = db.rawQuery(selectQuery, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {

                    Hysteria_P_TRANCE mHysteria_P_TRANCE = new Hysteria_P_TRANCE();
                    mHysteria_P_TRANCE.set_id(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE__id)));
                    mHysteria_P_TRANCE.setFitbit(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_fitbit)));
                    mHysteria_P_TRANCE.setSleep(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_sleep)));
                    mHysteria_P_TRANCE.setWeight(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_weight)));
                    mHysteria_P_TRANCE.setHeartrate(cur.getInt(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_heartrate)));
                    mHysteria_P_TRANCE.setDailyactivity(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_dailyactivity)));
                    mHysteria_P_TRANCE.setWaterintake(cur.getDouble(cur.getColumnIndex(FIELD_Hysteria_P_TRANCE_waterintake)));

                    list.add(mHysteria_P_TRANCE); // adding objects to the list

                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }



    /*
     *  getting Hysteria_P_TRANCE rows as mapped list for ListViews
     */
    public List<Map<String, String>> getListHysteria_P_TRANCE(List<Hysteria_P_TRANCE> list) { 

        List<Map<String, String>> mArrayList = new ArrayList<>();

        for (Hysteria_P_TRANCE mHysteria_P_TRANCE : list) {

            HashMap<String, String> map = new HashMap<>();

            map.put("Hysteria_P_TRANCE__id", String.valueOf(mHysteria_P_TRANCE.get_id()));
            map.put("Hysteria_P_TRANCE_fitbit", String.valueOf(mHysteria_P_TRANCE.getFitbit()));
            map.put("Hysteria_P_TRANCE_sleep", String.valueOf(mHysteria_P_TRANCE.getSleep()));
            map.put("Hysteria_P_TRANCE_weight", String.valueOf(mHysteria_P_TRANCE.getWeight()));
            map.put("Hysteria_P_TRANCE_heartrate", String.valueOf(mHysteria_P_TRANCE.getHeartrate()));
            map.put("Hysteria_P_TRANCE_dailyactivity", String.valueOf(mHysteria_P_TRANCE.getDailyactivity()));
            map.put("Hysteria_P_TRANCE_waterintake", String.valueOf(mHysteria_P_TRANCE.getWaterintake()));

            mArrayList.add(map);
        }

        return mArrayList;
    }



}